# Unity, Vuforia and iOS native subclass
-----------
This is a repository for **Unity 5.x**, **Vuforia 6.x** and **native iOS with XCode 8.1** subclassing example code in **Objective-C**.
I use **Unity 5.3.5p8**, **Vuforia 6.0.117** and **iOS 8.4 - 10.x**.
Some files change from Unity 4 to 5 and methods that call your subclass change too.
On Vuforia, it changed a lot, so, read this README.md file and comments on the examples inside the .h, .m and .mm files.

All these articles seams to be complicated, thats why i created this repository.
Bellow the help full links, is my step-by-step to create a **SIMPLE** integrantion and subclassing your Unity, Vuforia with iOS ( Objective-C ), for Swift, see the article bellow.

Usefull links that help-me to decide wich was better for me:

----------
**These integrations are different than mine, read them for better understanding and knowledge**

[The-Nerd-Be](https://the-nerd.be/2015/11/13/integrate-unity-5-in-a-native-ios-app-with-xcode-7/) Integrate Unity 5 in a native ios app with XCode 7 - 11/13/2015

[MakeTheGame](http://www.makethegame.net/unity/add-unity3d-to-native-ios-app-with-unity-5-and-vuforia-4-x/) Add Unity3D to native iOS app with Unity 5 and Vuforia 4.x

[GitHub Keyv](https://github.com/keyv/iOSUnityVuforiaGuide) Inspired on [The-Nerd-Be](https://the-nerd.be/2015/11/13/integrate-unity-5-in-a-native-ios-app-with-xcode-7/)

[BlitzAgency](https://github.com/blitzagency/ios-unity5) Inspired on [The-Nerd-Be](https://the-nerd.be/2015/11/13/integrate-unity-5-in-a-native-ios-app-with-xcode-7/) too and use Swift to it ( my next repo will be with Swift inspired on that article ).

*** These are more look like mine**

These two are old, but, for understand how to subclassing, read.

[Unity official forum](https://forum.unity3d.com/threads/unity-appcontroller-subclassing.191971/)

[Unity official forum](https://forum.unity3d.com/threads/unity-appcontroller-subclassing.191971/#post-1341666) post with example files ( old files ).

[Unity official forum](https://forum.unity3d.com/threads/unity-appcontroller-subclassing.191971/#post-1400785) my example come from better understanding from this post and adapted to my project.


##Step 1
Assuming you already create your Unity and Vuforia project, do:

1 - On Unity, select `File -> Build Settings...`; 

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/01.png?raw=true "")


2 - On the new window that appear, choose iOS and click on button `Switch Platform` ( if iOS is selected and this button is not clickable, jump to item 3;

3 - On the new window that appear, click on button `Player Settings...` ;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/02.png?raw=true "")


4 - On the right side or `Inspector` tab, with recommended configurations:

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/03.png?raw=true "")


*** - Change** `Company Name` **and** `Product Name`**;**

*** - The** `Resolution and Presentation`**, choose the best for your, in my case i choose just landscape.**

**ATTENTION HERE! Unity build with error in landscape mode, they need to fix, but you can fix it, just comment the line** `NSAssert(UnityShouldAutorotate(), @"UnityDefaultViewController should be used only if unity is set to autorotate");` **in file** `UnityViewControllerBaseiOS.mm`**;**

*** - REMEMBER to fix this line above later, when we import the example files;**

5 - MUST choose `OpenGLES2`;

6 - Change to your `Bundle Identifier`, `Version` and `Build`;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/04.png?raw=true "")


*** - The** `Icon` **and** `Splash Image`**, is up to you;**

*** - The** `Debugging and crash reporting` **i don't need to change;**

7 - Finished your settings, click on `Build`;

8 - Chose the folder name that will create the XCode project;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/05.png?raw=true "")



##Step 2
Your folder tree look like this:

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/06.png?raw=true "")


Now you will open your XCode project ( Assuming you already know XCode ), do:

1 - Choose your team to XCode register or verify if your bundle id is OK;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/07.png?raw=true "")


2 - On `info.plist` add `Privacy - Camera Usage Description` and add a description that appear when a alert pop to user ask him for permission camera;

3 - Build it on **your device** and see if the project will run with no errors; 

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/08.png?raw=true "")


**This is the first build: https://youtu.be/2f5tqsavBpo**

*** - My project is simple, i don't use plugins inside Unity or scripts deprecated;**

*** - If your project stop with errors, look for the error and try to fix it;**

*** - The link above ( Help links ), is to help you to know more about these errors;**

*** - Even there, you can't fix, let me a issue here and ASAP i have a break on my time, i will help you to find it on Google;**

4 - Test you app on your device, see if the Vuforia work as expected;

*** - Too many warnings, don't worry about it, most of them is depecrated because iOS 10 and XCode 8.1;**

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/09.png?raw=true "")


5 - Now you have to build your project inside this;


##Step 3
Lets do some "copy" and "paste" here! And USE MY **EXAMPLES FILES** to your project to follow.

**REMEMBER you can use this done project or you can create your own files based on this tutorial. Is up to you;**

1 - Comment out the line `IMPL_APP_CONTROLLER_SUBCLASS(VuforiaNativeRendererController)` on file `Libraries > Plugins > iOS > VuforiaNativeRendererController.mm`

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/10.png?raw=true "")


2 - Import the folder `Examples files` inside your project;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/11.png?raw=true "")


![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/12.png?raw=true "")


3 - Now we work inside this folder;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/13.png?raw=true "")


4 - Lets create a storyboard;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/14.png?raw=true "")


![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/15.png?raw=true "")


![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/17.png?raw=true "")


5 - In storyboard empty, drag and drop a Navigation Controller;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/18.png?raw=true "")


6 - Make the first controller, the Navigation Controller, a `Is Initial View Controller`;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/19.png?raw=true "")


7 - For this example, my firts view is a View Controller, so i delete the Table View Controller and drag and drop a View Controller there;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/20.png?raw=true "")


*** - Remember to make it the relation segue to a Navigation Controller as a Root View Controller;**

8 - On the this View Controller, select a `MainViewController` class and write `idMainViewController` in `Storyboard ID`;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/22.png?raw=true "")


9 - Create another View Controller to be the `ARViewController`;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/23.png?raw=true "")


10 - If you import my files, just make the connections. If not, create the connections. **Gray dots on line numbers**;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/24.png?raw=true "")


11 - Here don't have connection because i use segue navigation;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/25.png?raw=true "")


**JUST FOR KNOWLEDGE**

The button has the same level hierarchy than the `View To Unity` to make it above the view;

![](https://bitbucket.org/jack_loverde/unity-5-vuforia-6-and-ios-native-integration/raw/master/image%20source/26.png?raw=true "")



12 - Now you can build it in your device to test!

**This is the final build: https://youtu.be/Bq4K7uFpoWo**

13 - After that, then go creating your .h and .m files, like you normally do;


Author
------
Daniel Arantes Loverde - daniel@loverde.com.br

[![Alt text](https://docs.google.com/uc?id=0B4e4b1EGlPlzSi1yeXJ6aHFTRVE&amp;export=download "Jack Twitter")](http://twitter.com/jack_loverde)
[![Alt text](https://docs.google.com/uc?id=0B4e4b1EGlPlzcW9PMGlXLS0wRkU&amp;export=download "Jack Instagram")](https://instagram.com/loverde)
[![Alt text](https://docs.google.com/uc?id=0B4e4b1EGlPlzcDhLNzcwcE5nNzg&amp;export=download "Jack Linkedin")](https://br.linkedin.com/in/danieloverde)
[![Alt text](https://docs.google.com/uc?id=0B4e4b1EGlPlzcWtfOWR3d09aRjQ&amp;export=download "Jack Github")](https://github.com/jackloverde)
[![Alt text](https://docs.google.com/uc?id=0B4e4b1EGlPlzVTVNSE16OTM2Tms&amp;export=download "Jack Bitbucket")](https://bitbucket.org/jack_loverde)