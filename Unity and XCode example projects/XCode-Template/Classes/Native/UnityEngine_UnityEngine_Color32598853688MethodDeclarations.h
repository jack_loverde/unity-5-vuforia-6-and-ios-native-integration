﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// UnityEngine.Color32
struct Color32_t598853688;
struct Color32_t598853688_marshaled_pinvoke;
struct Color32_t598853688_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"

// System.String UnityEngine.Color32::ToString()
extern "C"  String_t* Color32_ToString_m909782902 (Color32_t598853688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Color32_t598853688;
struct Color32_t598853688_marshaled_pinvoke;

extern "C" void Color32_t598853688_marshal_pinvoke(const Color32_t598853688& unmarshaled, Color32_t598853688_marshaled_pinvoke& marshaled);
extern "C" void Color32_t598853688_marshal_pinvoke_back(const Color32_t598853688_marshaled_pinvoke& marshaled, Color32_t598853688& unmarshaled);
extern "C" void Color32_t598853688_marshal_pinvoke_cleanup(Color32_t598853688_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Color32_t598853688;
struct Color32_t598853688_marshaled_com;

extern "C" void Color32_t598853688_marshal_com(const Color32_t598853688& unmarshaled, Color32_t598853688_marshaled_com& marshaled);
extern "C" void Color32_t598853688_marshal_com_back(const Color32_t598853688_marshaled_com& marshaled, Color32_t598853688& unmarshaled);
extern "C" void Color32_t598853688_marshal_com_cleanup(Color32_t598853688_marshaled_com& marshaled);
