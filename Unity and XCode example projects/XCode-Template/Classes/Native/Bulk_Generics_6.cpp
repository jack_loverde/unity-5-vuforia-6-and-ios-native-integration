﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>
struct List_1_t595805021;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t159784161;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3065703549;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t2048428487;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3176762032;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t132831245;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerable_1_t2065558650;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t676510742;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ICollection_1_t3954202976;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1983528240;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t374511678;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerable_1_t2307239083;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t918191175;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ICollection_1_t4195883409;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t2088020251;
// System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>
struct List_1_t4242749885;
// System.Collections.Generic.IEnumerable`1<Vuforia.CameraDevice/CameraField>
struct IEnumerable_1_t1880509994;
// System.Collections.Generic.IEnumerator`1<Vuforia.CameraDevice/CameraField>
struct IEnumerator_1_t491462086;
// System.Collections.Generic.ICollection`1<Vuforia.CameraDevice/CameraField>
struct ICollection_1_t3769154320;
// Vuforia.CameraDevice/CameraField[]
struct CameraFieldU5BU5D_t3664195264;
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t1722560608;
// System.Collections.Generic.IEnumerable`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerable_1_t3655288013;
// System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerator_1_t2266240105;
// System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ICollection_1_t1248965043;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t882070065;
// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t682628370;
// System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerable_1_t2615355775;
// System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerator_1_t1226307867;
// System.Collections.Generic.ICollection`1<Vuforia.TargetFinder/TargetSearchResult>
struct ICollection_1_t209032805;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t1920244343;
// System.Collections.Generic.IEnumerable`1<Vuforia.VuforiaManagerImpl/IdPair>
struct IEnumerable_1_t2528532426;
// System.Collections.Generic.IEnumerator`1<Vuforia.VuforiaManagerImpl/IdPair>
struct IEnumerator_1_t1139484518;
// System.Collections.Generic.ICollection`1<Vuforia.VuforiaManagerImpl/IdPair>
struct ICollection_1_t122209456;
// Vuforia.VuforiaManagerImpl/IdPair[]
struct IdPairU5BU5D_t296135328;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t2974409999;
// System.Collections.ObjectModel.Collection`1<System.Object>
struct Collection_1_t3356274529;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t2570496278;
// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>
struct Collection_1_t2245071147;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>
struct IList_1_t1459292896;
// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>
struct Collection_1_t2486751580;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>
struct IList_1_t1700973329;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t1432926611;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ReadOnlyCollection_1_t321723229;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ReadOnlyCollection_1_t563403662;
// System.Comparison`1<System.Object>
struct Comparison_1_t2887177558;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// System.Converter`2<System.Object,System.Object>
struct Converter_2_t3715610867;
// System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649;
// System.String
struct String_t;
// System.Predicate`1<System.Object>
struct Predicate_1_t3781873254;
// System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Predicate_1_t6933607;
// System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetResultData>
struct Predicate_1_t2549574927;
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t2712548107;
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct StaticGetter_1_t3245381133;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t62446588;
// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
struct UnityEvent_2_t2262335274;
// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
struct UnityEvent_3_t1668724050;
// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
struct UnityEvent_4_t2186133700;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat615477791.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat615477791MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Collections_Generic_List_1_gen595805021.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3522586765.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException1794727681MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_ObjectDisposedException1794727681.h"
#include "mscorlib_System_Boolean476798718.h"
#include "Vuforia.UnityExtensions_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2541696822.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException1441619295.h"
#include "mscorlib_System_InvalidCastException241699085.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2541696822MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen132831245.h"
#include "mscorlib_System_Collections_Generic_List_1_gen132831245MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArg3059612989.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat152504015.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat152504015MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen374511678.h"
#include "mscorlib_System_Collections_Generic_List_1_gen374511678MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg3301293422.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat394184448.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat394184448MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4242749885.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4242749885MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer2874564333.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4262422655.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4262422655MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1722560608.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1722560608MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT354375056.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1742233378.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1742233378MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen682628370.h"
#include "mscorlib_System_Collections_Generic_List_1_gen682628370MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe3609410114.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat702301140.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat702301140MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen595805021MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat2532196025.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat2532196025MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen2974409999.h"
#include "System_System_Collections_Generic_Stack_1_gen2974409999MethodDeclarations.h"
#include "mscorlib_System_ArrayTypeMismatchException3681076194.h"
#include "mscorlib_System_Collections_ObjectModel_Collection3356274529.h"
#include "mscorlib_System_Collections_ObjectModel_Collection3356274529MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2245071147.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2245071147MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2486751580.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2486751580MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1432926611.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1432926611MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCol321723229.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCol321723229MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCol563403662.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCol563403662MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen2887177558.h"
#include "mscorlib_System_Comparison_1_gen2887177558MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "mscorlib_System_Converter_2_gen3715610867.h"
#include "mscorlib_System_Converter_2_gen3715610867MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateCastIt3934985649.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateCastIt3934985649MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked373807572MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_Nullable_1_gen497649510.h"
#include "mscorlib_System_Nullable_1_gen497649510MethodDeclarations.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_TimeSpan413522987MethodDeclarations.h"
#include "mscorlib_System_ValueType1744280289MethodDeclarations.h"
#include "mscorlib_System_ValueType1744280289.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen3781873254.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen6933607.h"
#include "mscorlib_System_Predicate_1_gen6933607MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_395876724.h"
#include "mscorlib_System_Predicate_1_gen2549574927.h"
#include "mscorlib_System_Predicate_1_gen2549574927MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2938518044.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_g2712548107.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_g2712548107MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGett3245381133.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGett3245381133MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen1152456458.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen1152456458MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen62446588.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen62446588MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase1020378628MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen2262335274.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen2262335274MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen1668724050.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen1668724050MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_gen2186133700.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_gen2186133700MethodDeclarations.h"

// System.Int32 System.Array::IndexOf<System.Int32>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisInt32_t1153838500_m877823422_gshared (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821* p0, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisInt32_t1153838500_m877823422(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3230847821*, int32_t, int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisInt32_t1153838500_m877823422_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<System.Int32>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisInt32_t1153838500_m3594335604_gshared (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3230847821** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisInt32_t1153838500_m3594335604(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3230847821**, int32_t, const MethodInfo*))Array_Resize_TisInt32_t1153838500_m3594335604_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2704617185_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* p0, Il2CppObject * p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2704617185(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Il2CppObject *, int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2704617185_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisIl2CppObject_m4097160425_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisIl2CppObject_m4097160425(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482**, int32_t, const MethodInfo*))Array_Resize_TisIl2CppObject_m4097160425_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t3059612989_m2624070686_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240* p0, CustomAttributeNamedArgument_t3059612989  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t3059612989_m2624070686(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240*, CustomAttributeNamedArgument_t3059612989 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t3059612989_m2624070686_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisCustomAttributeNamedArgument_t3059612989_m3334689556_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisCustomAttributeNamedArgument_t3059612989_m3334689556(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240**, int32_t, const MethodInfo*))Array_Resize_TisCustomAttributeNamedArgument_t3059612989_m3334689556_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t3301293422_m2421987343_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251* p0, CustomAttributeTypedArgument_t3301293422  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t3301293422_m2421987343(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251*, CustomAttributeTypedArgument_t3301293422 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t3301293422_m2421987343_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisCustomAttributeTypedArgument_t3301293422_m3172077765_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisCustomAttributeTypedArgument_t3301293422_m3172077765(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251**, int32_t, const MethodInfo*))Array_Resize_TisCustomAttributeTypedArgument_t3301293422_m3172077765_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<Vuforia.CameraDevice/CameraField>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisCameraField_t2874564333_m343808540_gshared (Il2CppObject * __this /* static, unused */, CameraFieldU5BU5D_t3664195264* p0, CameraField_t2874564333  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisCameraField_t2874564333_m343808540(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CameraFieldU5BU5D_t3664195264*, CameraField_t2874564333 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisCameraField_t2874564333_m343808540_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<Vuforia.CameraDevice/CameraField>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisCameraField_t2874564333_m6846162_gshared (Il2CppObject * __this /* static, unused */, CameraFieldU5BU5D_t3664195264** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisCameraField_t2874564333_m6846162(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, CameraFieldU5BU5D_t3664195264**, int32_t, const MethodInfo*))Array_Resize_TisCameraField_t2874564333_m6846162_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<Vuforia.Image/PIXEL_FORMAT>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisPIXEL_FORMAT_t354375056_m1758937983_gshared (Il2CppObject * __this /* static, unused */, PIXEL_FORMATU5BU5D_t882070065* p0, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisPIXEL_FORMAT_t354375056_m1758937983(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, PIXEL_FORMATU5BU5D_t882070065*, int32_t, int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisPIXEL_FORMAT_t354375056_m1758937983_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<Vuforia.Image/PIXEL_FORMAT>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisPIXEL_FORMAT_t354375056_m749834165_gshared (Il2CppObject * __this /* static, unused */, PIXEL_FORMATU5BU5D_t882070065** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisPIXEL_FORMAT_t354375056_m749834165(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, PIXEL_FORMATU5BU5D_t882070065**, int32_t, const MethodInfo*))Array_Resize_TisPIXEL_FORMAT_t354375056_m749834165_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<Vuforia.TargetFinder/TargetSearchResult>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisTargetSearchResult_t3609410114_m3197609661_gshared (Il2CppObject * __this /* static, unused */, TargetSearchResultU5BU5D_t1920244343* p0, TargetSearchResult_t3609410114  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisTargetSearchResult_t3609410114_m3197609661(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, TargetSearchResultU5BU5D_t1920244343*, TargetSearchResult_t3609410114 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisTargetSearchResult_t3609410114_m3197609661_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<Vuforia.TargetFinder/TargetSearchResult>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisTargetSearchResult_t3609410114_m4076314509_gshared (Il2CppObject * __this /* static, unused */, TargetSearchResultU5BU5D_t1920244343** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisTargetSearchResult_t3609410114_m4076314509(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, TargetSearchResultU5BU5D_t1920244343**, int32_t, const MethodInfo*))Array_Resize_TisTargetSearchResult_t3609410114_m4076314509_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<Vuforia.VuforiaManagerImpl/IdPair>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisIdPair_t3522586765_m750122696_gshared (Il2CppObject * __this /* static, unused */, IdPairU5BU5D_t296135328* p0, IdPair_t3522586765  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisIdPair_t3522586765_m750122696(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IdPairU5BU5D_t296135328*, IdPair_t3522586765 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisIdPair_t3522586765_m750122696_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<Vuforia.VuforiaManagerImpl/IdPair>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisIdPair_t3522586765_m3762909336_gshared (Il2CppObject * __this /* static, unused */, IdPairU5BU5D_t296135328** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisIdPair_t3522586765_m3762909336(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, IdPairU5BU5D_t296135328**, int32_t, const MethodInfo*))Array_Resize_TisIdPair_t3522586765_m3762909336_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VuforiaManagerImpl/IdPair>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m927528884_gshared (Enumerator_t615477791 * __this, List_1_t595805021 * ___l0, const MethodInfo* method)
{
	{
		List_1_t595805021 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t595805021 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m927528884_AdjustorThunk (Il2CppObject * __this, List_1_t595805021 * ___l0, const MethodInfo* method)
{
	Enumerator_t615477791 * _thisAdjusted = reinterpret_cast<Enumerator_t615477791 *>(__this + 1);
	Enumerator__ctor_m927528884(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m11475998_gshared (Enumerator_t615477791 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t615477791 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m11475998_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t615477791 * _thisAdjusted = reinterpret_cast<Enumerator_t615477791 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m11475998(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1815939914_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1815939914_gshared (Enumerator_t615477791 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1815939914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t615477791 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		IdPair_t3522586765  L_2 = (IdPair_t3522586765 )__this->get_current_3();
		IdPair_t3522586765  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1815939914_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t615477791 * _thisAdjusted = reinterpret_cast<Enumerator_t615477791 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1815939914(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VuforiaManagerImpl/IdPair>::Dispose()
extern "C"  void Enumerator_Dispose_m2942808729_gshared (Enumerator_t615477791 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t595805021 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2942808729_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t615477791 * _thisAdjusted = reinterpret_cast<Enumerator_t615477791 *>(__this + 1);
	Enumerator_Dispose_m2942808729(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VuforiaManagerImpl/IdPair>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m3789454674_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3789454674_gshared (Enumerator_t615477791 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3789454674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t595805021 * L_0 = (List_1_t595805021 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t615477791  L_1 = (*(Enumerator_t615477791 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t595805021 * L_7 = (List_1_t595805021 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3789454674_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t615477791 * _thisAdjusted = reinterpret_cast<Enumerator_t615477791 *>(__this + 1);
	Enumerator_VerifyState_m3789454674(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.VuforiaManagerImpl/IdPair>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1086239730_gshared (Enumerator_t615477791 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t615477791 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t595805021 * L_2 = (List_1_t595805021 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t595805021 * L_4 = (List_1_t595805021 *)__this->get_l_0();
		NullCheck(L_4);
		IdPairU5BU5D_t296135328* L_5 = (IdPairU5BU5D_t296135328*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		IdPair_t3522586765  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1086239730_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t615477791 * _thisAdjusted = reinterpret_cast<Enumerator_t615477791 *>(__this + 1);
	return Enumerator_MoveNext_m1086239730(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Vuforia.VuforiaManagerImpl/IdPair>::get_Current()
extern "C"  IdPair_t3522586765  Enumerator_get_Current_m210857566_gshared (Enumerator_t615477791 * __this, const MethodInfo* method)
{
	{
		IdPair_t3522586765  L_0 = (IdPair_t3522586765 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  IdPair_t3522586765  Enumerator_get_Current_m210857566_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t615477791 * _thisAdjusted = reinterpret_cast<Enumerator_t615477791 *>(__this + 1);
	return Enumerator_get_Current_m210857566(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m1394852971_gshared (List_1_t2522024052 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Int32U5BU5D_t3230847821* L_0 = ((List_1_t2522024052_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1198742556_gshared (List_1_t2522024052 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t2522024052 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Int32U5BU5D_t3230847821* L_3 = ((List_1_t2522024052_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t2522024052 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((Int32U5BU5D_t3230847821*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t2522024052 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m2126972244_MetadataUsageId;
extern "C"  void List_1__ctor_m2126972244_gshared (List_1_t2522024052 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m2126972244_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((Int32U5BU5D_t3230847821*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.cctor()
extern "C"  void List_1__cctor_m1113350026_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t2522024052_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((Int32U5BU5D_t3230847821*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1918002125_gshared (List_1_t2522024052 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t2522024052 *)__this);
		Enumerator_t2541696822  L_0 = ((  Enumerator_t2541696822  (*) (List_1_t2522024052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2522024052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t2541696822  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1952216609_gshared (List_1_t2522024052 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m868394352_gshared (List_1_t2522024052 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t2522024052 *)__this);
		Enumerator_t2541696822  L_0 = ((  Enumerator_t2541696822  (*) (List_1_t2522024052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2522024052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t2541696822  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m3146823117_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m3146823117_gshared (List_1_t2522024052 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m3146823117_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t2522024052 *)__this);
			((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m474280595_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m474280595_gshared (List_1_t2522024052 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m474280595_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t2522024052 *)__this);
			bool L_1 = ((  bool (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m615988645_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m615988645_gshared (List_1_t2522024052 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m615988645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t2522024052 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m411008408_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m411008408_gshared (List_1_t2522024052 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m411008408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t2522024052 *)__this);
			((  void (*) (List_1_t2522024052 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)L_1, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m1912599696_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m1912599696_gshared (List_1_t2522024052 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m1912599696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t2522024052 *)__this);
			((  bool (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3879536340_gshared (List_1_t2522024052 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m857080475_gshared (List_1_t2522024052 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m86227298_gshared (List_1_t2522024052 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t2522024052 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m456041327_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m456041327_gshared (List_1_t2522024052 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m456041327_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t2522024052 *)__this);
			((  void (*) (List_1_t2522024052 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)L_0, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(T)
extern "C"  void List_1_Add_m1089213787_gshared (List_1_t2522024052 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		Int32U5BU5D_t3230847821* L_2 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		int32_t L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1518088151_gshared (List_1_t2522024052 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		Int32U5BU5D_t3230847821* L_3 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t2522024052 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t2522024052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t2522024052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3810486997_gshared (List_1_t2522024052 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< Int32U5BU5D_t3230847821*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Int32>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (Int32U5BU5D_t3230847821*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m3071459221_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m3071459221_gshared (List_1_t2522024052 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m3071459221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject* V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Int32>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (int32_t)L_3;
			int32_t L_4 = V_0;
			NullCheck((List_1_t2522024052 *)__this);
			((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3137340898_gshared (List_1_t2522024052 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t2522024052 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t2522024052 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t2522024052 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C"  void List_1_Clear_m3095953558_gshared (List_1_t2522024052 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(T)
extern "C"  bool List_1_Contains_m3175073495_gshared (List_1_t2522024052 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		int32_t L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3230847821*, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (Int32U5BU5D_t3230847821*)L_0, (int32_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1849335948_gshared (List_1_t2522024052 * __this, Int32U5BU5D_t3230847821* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		Int32U5BU5D_t3230847821* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t2541696822  List_1_GetEnumerator_m1383295158_gshared (List_1_t2522024052 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2541696822  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1242988386(&L_0, (List_1_t2522024052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3246901839_gshared (List_1_t2522024052 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		int32_t L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3230847821*, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (Int32U5BU5D_t3230847821*)L_0, (int32_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3680447203_gshared (List_1_t2522024052 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		int32_t L_6 = ___start0;
		Int32U5BU5D_t3230847821* L_7 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_15 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m1595703132_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m1595703132_gshared (List_1_t2522024052 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m1595703132_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2397006339_gshared (List_1_t2522024052 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		Int32U5BU5D_t3230847821* L_2 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		Int32U5BU5D_t3230847821* L_4 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		int32_t L_5 = ___index0;
		int32_t L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m3092536376_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m3092536376_gshared (List_1_t2522024052 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m3092536376_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(T)
extern "C"  bool List_1_Remove_m4061378620_gshared (List_1_t2522024052 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___item0;
		NullCheck((List_1_t2522024052 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m270859209_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m270859209_gshared (List_1_t2522024052 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m270859209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C"  Int32U5BU5D_t3230847821* List_1_ToArray_m1209652252_gshared (List_1_t2522024052 * __this, const MethodInfo* method)
{
	Int32U5BU5D_t3230847821* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (Int32U5BU5D_t3230847821*)((Int32U5BU5D_t3230847821*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		Int32U5BU5D_t3230847821* L_1 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		Int32U5BU5D_t3230847821* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2719438728_gshared (List_1_t2522024052 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3230847821* L_0 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m3023244265_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m3023244265_gshared (List_1_t2522024052 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m3023244265_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		Int32U5BU5D_t3230847821** L_3 = (Int32U5BU5D_t3230847821**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3230847821**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)(NULL /*static, unused*/, (Int32U5BU5D_t3230847821**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C"  int32_t List_1_get_Count_m2520315171_gshared (List_1_t2522024052 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m18886394_MetadataUsageId;
extern "C"  int32_t List_1_get_Item_m18886394_gshared (List_1_t2522024052 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m18886394_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		Int32U5BU5D_t3230847821* L_3 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m3230217754_MetadataUsageId;
extern "C"  void List_1_set_Item_m3230217754_gshared (List_1_t2522024052 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m3230217754_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t2522024052 *)__this);
		((  void (*) (List_1_t2522024052 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t2522024052 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		Int32U5BU5D_t3230847821* L_4 = (Int32U5BU5D_t3230847821*)__this->get__items_1();
		int32_t L_5 = ___index0;
		int32_t L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m3048469268_gshared (List_1_t1244034627 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t1108656482* L_0 = ((List_1_t1244034627_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1160795371_gshared (List_1_t1244034627 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t1244034627 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t1108656482* L_3 = ((List_1_t1244034627_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t1244034627 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t1244034627 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m3643386469_MetadataUsageId;
extern "C"  void List_1__ctor_m3643386469_gshared (List_1_t1244034627 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m3643386469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C"  void List_1__cctor_m3826137881_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t1244034627_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared (List_1_t1244034627 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t1244034627 *)__this);
		Enumerator_t1263707397  L_0 = ((  Enumerator_t1263707397  (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1244034627 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t1263707397  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared (List_1_t1244034627 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared (List_1_t1244034627 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t1244034627 *)__this);
		Enumerator_t1263707397  L_0 = ((  Enumerator_t1263707397  (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1244034627 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t1263707397  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m3794749222_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m3794749222_gshared (List_1_t1244034627 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m3794749222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1244034627 *)__this);
			((  void (*) (List_1_t1244034627 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1244034627 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m2659633254_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m2659633254_gshared (List_1_t1244034627 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m2659633254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1244034627 *)__this);
			bool L_1 = ((  bool (*) (List_1_t1244034627 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t1244034627 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m3431692926_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3431692926_gshared (List_1_t1244034627 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m3431692926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1244034627 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t1244034627 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1244034627 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m2067529129_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m2067529129_gshared (List_1_t1244034627 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m2067529129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1244034627 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t1244034627 *)__this);
			((  void (*) (List_1_t1244034627 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t1244034627 *)__this, (int32_t)L_1, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m1644145887_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m1644145887_gshared (List_1_t1244034627 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m1644145887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1244034627 *)__this);
			((  bool (*) (List_1_t1244034627 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t1244034627 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared (List_1_t1244034627 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared (List_1_t1244034627 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3985478825_gshared (List_1_t1244034627 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1244034627 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t1244034627 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_1;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m3234554688_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m3234554688_gshared (List_1_t1244034627 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m3234554688_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t1244034627 *)__this);
			((  void (*) (List_1_t1244034627 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t1244034627 *)__this, (int32_t)L_0, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C"  void List_1_Add_m642669291_gshared (List_1_t1244034627 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		ObjectU5BU5D_t1108656482* L_1 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1244034627 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		Il2CppObject * L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m4122600870_gshared (List_1_t1244034627 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t1244034627 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t1244034627 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t1244034627 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2478449828_gshared (List_1_t1244034627 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1244034627 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		ObjectU5BU5D_t1108656482* L_5 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< ObjectU5BU5D_t1108656482*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (ObjectU5BU5D_t1108656482*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m1739422052_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m1739422052_gshared (List_1_t1244034627 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m1739422052_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (Il2CppObject *)L_3;
			Il2CppObject * L_4 = V_0;
			NullCheck((List_1_t1244034627 *)__this);
			((  void (*) (List_1_t1244034627 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1244034627 *)__this, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2229151411_gshared (List_1_t1244034627 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t1244034627 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t1244034627 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t1244034627 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m454602559_gshared (List_1_t1244034627 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		ObjectU5BU5D_t1108656482* L_1 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C"  bool List_1_Contains_m4186092781_gshared (List_1_t1244034627 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Il2CppObject *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t1108656482*)L_0, (Il2CppObject *)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3988356635_gshared (List_1_t1244034627 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		ObjectU5BU5D_t1108656482* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1263707397  List_1_GetEnumerator_m2326457258_gshared (List_1_t1244034627 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1263707397  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1029849669(&L_0, (List_1_t1244034627 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1752303327_gshared (List_1_t1244034627 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, Il2CppObject *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t1108656482*)L_0, (Il2CppObject *)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3807054194_gshared (List_1_t1244034627 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_5 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		int32_t L_6 = ___start0;
		ObjectU5BU5D_t1108656482* L_7 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_15 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m3734723819_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m3734723819_gshared (List_1_t1244034627 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m3734723819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3427163986_gshared (List_1_t1244034627 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1244034627 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1244034627 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t1244034627 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		ObjectU5BU5D_t1108656482* L_4 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		int32_t L_5 = ___index0;
		Il2CppObject * L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m2905071175_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m2905071175_gshared (List_1_t1244034627 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m2905071175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C"  bool List_1_Remove_m2747911208_gshared (List_1_t1244034627 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t1244034627 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t1244034627 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1244034627 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t1244034627 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m1301016856_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m1301016856_gshared (List_1_t1244034627 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m1301016856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t1244034627 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		ObjectU5BU5D_t1108656482* L_5 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t1108656482* List_1_ToArray_m238588755_gshared (List_1_t1244034627 * __this, const MethodInfo* method)
{
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		ObjectU5BU5D_t1108656482* L_1 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		ObjectU5BU5D_t1108656482* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m543520655_gshared (List_1_t1244034627 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m1332789688_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m1332789688_gshared (List_1_t1244034627 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m1332789688_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		ObjectU5BU5D_t1108656482** L_3 = (ObjectU5BU5D_t1108656482**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t1108656482**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2599103100_gshared (List_1_t1244034627 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m2771401372_MetadataUsageId;
extern "C"  Il2CppObject * List_1_get_Item_m2771401372_gshared (List_1_t1244034627 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m2771401372_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m1074271145_MetadataUsageId;
extern "C"  void List_1_set_Item_m1074271145_gshared (List_1_t1244034627 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m1074271145_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1244034627 *)__this);
		((  void (*) (List_1_t1244034627 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1244034627 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		ObjectU5BU5D_t1108656482* L_4 = (ObjectU5BU5D_t1108656482*)__this->get__items_1();
		int32_t L_5 = ___index0;
		Il2CppObject * L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void List_1__ctor_m3182785955_gshared (List_1_t132831245 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = ((List_1_t132831245_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2518707964_gshared (List_1_t132831245 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t132831245 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_3 = ((List_1_t132831245_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t132831245 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((CustomAttributeNamedArgumentU5BU5D_t1983528240*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t132831245 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m2888355444_MetadataUsageId;
extern "C"  void List_1__ctor_m2888355444_gshared (List_1_t132831245 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m2888355444_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((CustomAttributeNamedArgumentU5BU5D_t1983528240*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern "C"  void List_1__cctor_m3694987882_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t132831245_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((CustomAttributeNamedArgumentU5BU5D_t1983528240*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1445350893_gshared (List_1_t132831245 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t132831245 *)__this);
		Enumerator_t152504015  L_0 = ((  Enumerator_t152504015  (*) (List_1_t132831245 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t132831245 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t152504015  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m499056897_gshared (List_1_t132831245 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1886158288_gshared (List_1_t132831245 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t132831245 *)__this);
		Enumerator_t152504015  L_0 = ((  Enumerator_t152504015  (*) (List_1_t132831245 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t132831245 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t152504015  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m225941485_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m225941485_gshared (List_1_t132831245 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m225941485_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t132831245 *)__this);
			((  void (*) (List_1_t132831245 *, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t132831245 *)__this, (CustomAttributeNamedArgument_t3059612989 )((*(CustomAttributeNamedArgument_t3059612989 *)((CustomAttributeNamedArgument_t3059612989 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m3549523443_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m3549523443_gshared (List_1_t132831245 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m3549523443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t132831245 *)__this);
			bool L_1 = ((  bool (*) (List_1_t132831245 *, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t132831245 *)__this, (CustomAttributeNamedArgument_t3059612989 )((*(CustomAttributeNamedArgument_t3059612989 *)((CustomAttributeNamedArgument_t3059612989 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m2250248133_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2250248133_gshared (List_1_t132831245 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m2250248133_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t132831245 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t132831245 *, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t132831245 *)__this, (CustomAttributeNamedArgument_t3059612989 )((*(CustomAttributeNamedArgument_t3059612989 *)((CustomAttributeNamedArgument_t3059612989 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m3869877944_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m3869877944_gshared (List_1_t132831245 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m3869877944_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t132831245 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t132831245 *)__this);
			((  void (*) (List_1_t132831245 *, int32_t, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t132831245 *)__this, (int32_t)L_1, (CustomAttributeNamedArgument_t3059612989 )((*(CustomAttributeNamedArgument_t3059612989 *)((CustomAttributeNamedArgument_t3059612989 *)UnBox (L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m686389104_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m686389104_gshared (List_1_t132831245 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m686389104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t132831245 *)__this);
			((  bool (*) (List_1_t132831245 *, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t132831245 *)__this, (CustomAttributeNamedArgument_t3059612989 )((*(CustomAttributeNamedArgument_t3059612989 *)((CustomAttributeNamedArgument_t3059612989 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2804861492_gshared (List_1_t132831245 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3955324539_gshared (List_1_t132831245 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1086436674_gshared (List_1_t132831245 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t132831245 *)__this);
		CustomAttributeNamedArgument_t3059612989  L_1 = ((  CustomAttributeNamedArgument_t3059612989  (*) (List_1_t132831245 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t132831245 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		CustomAttributeNamedArgument_t3059612989  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m124978319_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m124978319_gshared (List_1_t132831245 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m124978319_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t132831245 *)__this);
			((  void (*) (List_1_t132831245 *, int32_t, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t132831245 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t3059612989 )((*(CustomAttributeNamedArgument_t3059612989 *)((CustomAttributeNamedArgument_t3059612989 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void List_1_Add_m1339738748_gshared (List_1_t132831245 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_1 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t132831245 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_2 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		CustomAttributeNamedArgument_t3059612989  L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeNamedArgument_t3059612989 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1448862391_gshared (List_1_t132831245 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_3 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t132831245 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t132831245 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t132831245 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t132831245 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3229326773_gshared (List_1_t132831245 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t132831245 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_5 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< CustomAttributeNamedArgumentU5BU5D_t1983528240*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m2490298997_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m2490298997_gshared (List_1_t132831245 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m2490298997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeNamedArgument_t3059612989  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			CustomAttributeNamedArgument_t3059612989  L_3 = InterfaceFuncInvoker0< CustomAttributeNamedArgument_t3059612989  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (CustomAttributeNamedArgument_t3059612989 )L_3;
			CustomAttributeNamedArgument_t3059612989  L_4 = V_0;
			NullCheck((List_1_t132831245 *)__this);
			((  void (*) (List_1_t132831245 *, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t132831245 *)__this, (CustomAttributeNamedArgument_t3059612989 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1506248450_gshared (List_1_t132831245 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t132831245 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t132831245 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t132831245 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void List_1_Clear_m588919246_gshared (List_1_t132831245 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_1 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool List_1_Contains_m3793098560_gshared (List_1_t132831245 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		CustomAttributeNamedArgument_t3059612989  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240*, CustomAttributeNamedArgument_t3059612989 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_0, (CustomAttributeNamedArgument_t3059612989 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2854849388_gshared (List_1_t132831245 * __this, CustomAttributeNamedArgumentU5BU5D_t1983528240* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Enumerator_t152504015  List_1_GetEnumerator_m712489085_gshared (List_1_t132831245 * __this, const MethodInfo* method)
{
	{
		Enumerator_t152504015  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m256124610(&L_0, (List_1_t132831245 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m33596728_gshared (List_1_t132831245 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		CustomAttributeNamedArgument_t3059612989  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240*, CustomAttributeNamedArgument_t3059612989 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_0, (CustomAttributeNamedArgument_t3059612989 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2460300739_gshared (List_1_t132831245 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_5 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		int32_t L_6 = ___start0;
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_7 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_15 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m2601216572_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m2601216572_gshared (List_1_t132831245 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m2601216572_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3041627363_gshared (List_1_t132831245 * __this, int32_t ___index0, CustomAttributeNamedArgument_t3059612989  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t132831245 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_2 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t132831245 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t132831245 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_4 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeNamedArgument_t3059612989  L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeNamedArgument_t3059612989 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m2943309592_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m2943309592_gshared (List_1_t132831245 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m2943309592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool List_1_Remove_m1013425979_gshared (List_1_t132831245 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		CustomAttributeNamedArgument_t3059612989  L_0 = ___item0;
		NullCheck((List_1_t132831245 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t132831245 *, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t132831245 *)__this, (CustomAttributeNamedArgument_t3059612989 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t132831245 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m915480233_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m915480233_gshared (List_1_t132831245 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m915480233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t132831245 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_5 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::ToArray()
extern "C"  CustomAttributeNamedArgumentU5BU5D_t1983528240* List_1_ToArray_m1013706236_gshared (List_1_t132831245 * __this, const MethodInfo* method)
{
	CustomAttributeNamedArgumentU5BU5D_t1983528240* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)((CustomAttributeNamedArgumentU5BU5D_t1983528240*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_1 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m4200284520_gshared (List_1_t132831245 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_0 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m2954018505_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m2954018505_gshared (List_1_t132831245 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m2954018505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240** L_3 = (CustomAttributeNamedArgumentU5BU5D_t1983528240**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t1983528240**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t1983528240**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t List_1_get_Count_m858806083_gshared (List_1_t132831245 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m3808740495_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t3059612989  List_1_get_Item_m3808740495_gshared (List_1_t132831245 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m3808740495_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_3 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t3059612989  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m4235731194_MetadataUsageId;
extern "C"  void List_1_set_Item_m4235731194_gshared (List_1_t132831245 * __this, int32_t ___index0, CustomAttributeNamedArgument_t3059612989  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m4235731194_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t132831245 *)__this);
		((  void (*) (List_1_t132831245 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t132831245 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_4 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeNamedArgument_t3059612989  L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeNamedArgument_t3059612989 )L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void List_1__ctor_m1026780308_gshared (List_1_t374511678 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = ((List_1_t374511678_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3629378411_gshared (List_1_t374511678 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t374511678 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_3 = ((List_1_t374511678_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t374511678 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((CustomAttributeTypedArgumentU5BU5D_t2088020251*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t374511678 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m1237246949_MetadataUsageId;
extern "C"  void List_1__ctor_m1237246949_gshared (List_1_t374511678 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m1237246949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((CustomAttributeTypedArgumentU5BU5D_t2088020251*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern "C"  void List_1__cctor_m1283322265_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t374511678_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((CustomAttributeTypedArgumentU5BU5D_t2088020251*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3773941406_gshared (List_1_t374511678 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t374511678 *)__this);
		Enumerator_t394184448  L_0 = ((  Enumerator_t394184448  (*) (List_1_t374511678 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t374511678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t394184448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1212976944_gshared (List_1_t374511678 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1995803071_gshared (List_1_t374511678 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t374511678 *)__this);
		Enumerator_t394184448  L_0 = ((  Enumerator_t394184448  (*) (List_1_t374511678 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t374511678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t394184448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m414231134_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m414231134_gshared (List_1_t374511678 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m414231134_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t374511678 *)__this);
			((  void (*) (List_1_t374511678 *, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t374511678 *)__this, (CustomAttributeTypedArgument_t3301293422 )((*(CustomAttributeTypedArgument_t3301293422 *)((CustomAttributeTypedArgument_t3301293422 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m1543977506_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m1543977506_gshared (List_1_t374511678 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m1543977506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t374511678 *)__this);
			bool L_1 = ((  bool (*) (List_1_t374511678 *, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t374511678 *)__this, (CustomAttributeTypedArgument_t3301293422 )((*(CustomAttributeTypedArgument_t3301293422 *)((CustomAttributeTypedArgument_t3301293422 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m1354269110_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1354269110_gshared (List_1_t374511678 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m1354269110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t374511678 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t374511678 *, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t374511678 *)__this, (CustomAttributeTypedArgument_t3301293422 )((*(CustomAttributeTypedArgument_t3301293422 *)((CustomAttributeTypedArgument_t3301293422 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m3967399721_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m3967399721_gshared (List_1_t374511678 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m3967399721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t374511678 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t374511678 *)__this);
			((  void (*) (List_1_t374511678 *, int32_t, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t374511678 *)__this, (int32_t)L_1, (CustomAttributeTypedArgument_t3301293422 )((*(CustomAttributeTypedArgument_t3301293422 *)((CustomAttributeTypedArgument_t3301293422 *)UnBox (L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m796033887_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m796033887_gshared (List_1_t374511678 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m796033887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t374511678 *)__this);
			((  bool (*) (List_1_t374511678 *, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t374511678 *)__this, (CustomAttributeTypedArgument_t3301293422 )((*(CustomAttributeTypedArgument_t3301293422 *)((CustomAttributeTypedArgument_t3301293422 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1976723363_gshared (List_1_t374511678 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m218083500_gshared (List_1_t374511678 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m190457651_gshared (List_1_t374511678 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t374511678 *)__this);
		CustomAttributeTypedArgument_t3301293422  L_1 = ((  CustomAttributeTypedArgument_t3301293422  (*) (List_1_t374511678 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t374511678 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		CustomAttributeTypedArgument_t3301293422  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m3649092800_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m3649092800_gshared (List_1_t374511678 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m3649092800_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t374511678 *)__this);
			((  void (*) (List_1_t374511678 *, int32_t, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t374511678 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t3301293422 )((*(CustomAttributeTypedArgument_t3301293422 *)((CustomAttributeTypedArgument_t3301293422 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void List_1_Add_m1547284843_gshared (List_1_t374511678 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_1 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t374511678 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_2 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		CustomAttributeTypedArgument_t3301293422  L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeTypedArgument_t3301293422 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3071867942_gshared (List_1_t374511678 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_3 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t374511678 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t374511678 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t374511678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t374511678 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2401188644_gshared (List_1_t374511678 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t374511678 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_5 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< CustomAttributeTypedArgumentU5BU5D_t2088020251*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m1662160868_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m1662160868_gshared (List_1_t374511678 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m1662160868_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeTypedArgument_t3301293422  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			CustomAttributeTypedArgument_t3301293422  L_3 = InterfaceFuncInvoker0< CustomAttributeTypedArgument_t3301293422  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (CustomAttributeTypedArgument_t3301293422 )L_3;
			CustomAttributeTypedArgument_t3301293422  L_4 = V_0;
			NullCheck((List_1_t374511678 *)__this);
			((  void (*) (List_1_t374511678 *, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t374511678 *)__this, (CustomAttributeTypedArgument_t3301293422 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1061486643_gshared (List_1_t374511678 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t374511678 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t374511678 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t374511678 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void List_1_Clear_m2727880895_gshared (List_1_t374511678 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_1 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool List_1_Contains_m4075630001_gshared (List_1_t374511678 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		CustomAttributeTypedArgument_t3301293422  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251*, CustomAttributeTypedArgument_t3301293422 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_0, (CustomAttributeTypedArgument_t3301293422 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2968269979_gshared (List_1_t374511678 * __this, CustomAttributeTypedArgumentU5BU5D_t2088020251* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Enumerator_t394184448  List_1_GetEnumerator_m873213550_gshared (List_1_t374511678 * __this, const MethodInfo* method)
{
	{
		Enumerator_t394184448  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m444414259(&L_0, (List_1_t374511678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1705278631_gshared (List_1_t374511678 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		CustomAttributeTypedArgument_t3301293422  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251*, CustomAttributeTypedArgument_t3301293422 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_0, (CustomAttributeTypedArgument_t3301293422 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m490684402_gshared (List_1_t374511678 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_5 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		int32_t L_6 = ___start0;
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_7 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_15 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m2714637163_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m2714637163_gshared (List_1_t374511678 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m2714637163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m833926610_gshared (List_1_t374511678 * __this, int32_t ___index0, CustomAttributeTypedArgument_t3301293422  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t374511678 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_2 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t374511678 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t374511678 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_4 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeTypedArgument_t3301293422  L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeTypedArgument_t3301293422 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m1671517383_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m1671517383_gshared (List_1_t374511678 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m1671517383_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool List_1_Remove_m3561203180_gshared (List_1_t374511678 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		CustomAttributeTypedArgument_t3301293422  L_0 = ___item0;
		NullCheck((List_1_t374511678 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t374511678 *, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t374511678 *)__this, (CustomAttributeTypedArgument_t3301293422 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t374511678 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m3002746776_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m3002746776_gshared (List_1_t374511678 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m3002746776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t374511678 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_5 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::ToArray()
extern "C"  CustomAttributeTypedArgumentU5BU5D_t2088020251* List_1_ToArray_m3561483437_gshared (List_1_t374511678 * __this, const MethodInfo* method)
{
	CustomAttributeTypedArgumentU5BU5D_t2088020251* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)((CustomAttributeTypedArgumentU5BU5D_t2088020251*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_1 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2958543191_gshared (List_1_t374511678 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_0 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m282056760_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m282056760_gshared (List_1_t374511678 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m282056760_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251** L_3 = (CustomAttributeTypedArgumentU5BU5D_t2088020251**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t2088020251**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t2088020251**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t List_1_get_Count_m1141337524_gshared (List_1_t374511678 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m1601039742_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t3301293422  List_1_get_Item_m1601039742_gshared (List_1_t374511678 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m1601039742_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_3 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t3301293422  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m54184489_MetadataUsageId;
extern "C"  void List_1_set_Item_m54184489_gshared (List_1_t374511678 * __this, int32_t ___index0, CustomAttributeTypedArgument_t3301293422  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m54184489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t374511678 *)__this);
		((  void (*) (List_1_t374511678 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t374511678 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_4 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeTypedArgument_t3301293422  L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeTypedArgument_t3301293422 )L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::.ctor()
extern "C"  void List_1__ctor_m551957065_gshared (List_1_t4242749885 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		CameraFieldU5BU5D_t3664195264* L_0 = ((List_1_t4242749885_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1519168382_gshared (List_1_t4242749885 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t4242749885 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		CameraFieldU5BU5D_t3664195264* L_3 = ((List_1_t4242749885_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t4242749885 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CameraDevice/CameraField>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((CameraFieldU5BU5D_t3664195264*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t4242749885 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m3776092722_MetadataUsageId;
extern "C"  void List_1__ctor_m3776092722_gshared (List_1_t4242749885 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m3776092722_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((CameraFieldU5BU5D_t3664195264*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::.cctor()
extern "C"  void List_1__cctor_m3384617324_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t4242749885_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((CameraFieldU5BU5D_t3664195264*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m283261995_gshared (List_1_t4242749885 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t4242749885 *)__this);
		Enumerator_t4262422655  L_0 = ((  Enumerator_t4262422655  (*) (List_1_t4242749885 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t4242749885 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t4262422655  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3279827459_gshared (List_1_t4242749885 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		CameraFieldU5BU5D_t3664195264* L_0 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1006370002_gshared (List_1_t4242749885 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t4242749885 *)__this);
		Enumerator_t4262422655  L_0 = ((  Enumerator_t4262422655  (*) (List_1_t4242749885 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t4242749885 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t4262422655  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m3797740203_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m3797740203_gshared (List_1_t4242749885 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m3797740203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t4242749885 *)__this);
			((  void (*) (List_1_t4242749885 *, CameraField_t2874564333 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t4242749885 *)__this, (CameraField_t2874564333 )((*(CameraField_t2874564333 *)((CameraField_t2874564333 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m3872630645_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m3872630645_gshared (List_1_t4242749885 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m3872630645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t4242749885 *)__this);
			bool L_1 = ((  bool (*) (List_1_t4242749885 *, CameraField_t2874564333 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t4242749885 *)__this, (CameraField_t2874564333 )((*(CameraField_t2874564333 *)((CameraField_t2874564333 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m4001485699_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m4001485699_gshared (List_1_t4242749885 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m4001485699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t4242749885 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t4242749885 *, CameraField_t2874564333 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t4242749885 *)__this, (CameraField_t2874564333 )((*(CameraField_t2874564333 *)((CameraField_t2874564333 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m2676776054_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m2676776054_gshared (List_1_t4242749885 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m2676776054_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t4242749885 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t4242749885 *)__this);
			((  void (*) (List_1_t4242749885 *, int32_t, CameraField_t2874564333 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t4242749885 *)__this, (int32_t)L_1, (CameraField_t2874564333 )((*(CameraField_t2874564333 *)((CameraField_t2874564333 *)UnBox (L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m2791064818_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m2791064818_gshared (List_1_t4242749885 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m2791064818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t4242749885 *)__this);
			((  bool (*) (List_1_t4242749885 *, CameraField_t2874564333 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t4242749885 *)__this, (CameraField_t2874564333 )((*(CameraField_t2874564333 *)((CameraField_t2874564333 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2548074806_gshared (List_1_t4242749885 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1450652025_gshared (List_1_t4242749885 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3572803776_gshared (List_1_t4242749885 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t4242749885 *)__this);
		CameraField_t2874564333  L_1 = ((  CameraField_t2874564333  (*) (List_1_t4242749885 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t4242749885 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		CameraField_t2874564333  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m310330061_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m310330061_gshared (List_1_t4242749885 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m310330061_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t4242749885 *)__this);
			((  void (*) (List_1_t4242749885 *, int32_t, CameraField_t2874564333 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t4242749885 *)__this, (int32_t)L_0, (CameraField_t2874564333 )((*(CameraField_t2874564333 *)((CameraField_t2874564333 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::Add(T)
extern "C"  void List_1_Add_m246317881_gshared (List_1_t4242749885 * __this, CameraField_t2874564333  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		CameraFieldU5BU5D_t3664195264* L_1 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t4242749885 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		CameraFieldU5BU5D_t3664195264* L_2 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		CameraField_t2874564333  L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CameraField_t2874564333 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m554108217_gshared (List_1_t4242749885 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		CameraFieldU5BU5D_t3664195264* L_3 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t4242749885 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t4242749885 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t4242749885 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t4242749885 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2913744951_gshared (List_1_t4242749885 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CameraDevice/CameraField>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t4242749885 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		CameraFieldU5BU5D_t3664195264* L_5 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< CameraFieldU5BU5D_t3664195264*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<Vuforia.CameraDevice/CameraField>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (CameraFieldU5BU5D_t3664195264*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m2174717175_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m2174717175_gshared (List_1_t4242749885 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m2174717175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CameraField_t2874564333  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CameraDevice/CameraField>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			CameraField_t2874564333  L_3 = InterfaceFuncInvoker0< CameraField_t2874564333  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<Vuforia.CameraDevice/CameraField>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (CameraField_t2874564333 )L_3;
			CameraField_t2874564333  L_4 = V_0;
			NullCheck((List_1_t4242749885 *)__this);
			((  void (*) (List_1_t4242749885 *, CameraField_t2874564333 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t4242749885 *)__this, (CameraField_t2874564333 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1230824256_gshared (List_1_t4242749885 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t4242749885 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t4242749885 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t4242749885 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::Clear()
extern "C"  void List_1_Clear_m24717964_gshared (List_1_t4242749885 * __this, const MethodInfo* method)
{
	{
		CameraFieldU5BU5D_t3664195264* L_0 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		CameraFieldU5BU5D_t3664195264* L_1 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::Contains(T)
extern "C"  bool List_1_Contains_m119991422_gshared (List_1_t4242749885 * __this, CameraField_t2874564333  ___item0, const MethodInfo* method)
{
	{
		CameraFieldU5BU5D_t3664195264* L_0 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		CameraField_t2874564333  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CameraFieldU5BU5D_t3664195264*, CameraField_t2874564333 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (CameraFieldU5BU5D_t3664195264*)L_0, (CameraField_t2874564333 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3403638382_gshared (List_1_t4242749885 * __this, CameraFieldU5BU5D_t3664195264* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		CameraFieldU5BU5D_t3664195264* L_0 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		CameraFieldU5BU5D_t3664195264* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::GetEnumerator()
extern "C"  Enumerator_t4262422655  List_1_GetEnumerator_m2271362747_gshared (List_1_t4242749885 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4262422655  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m2176291008(&L_0, (List_1_t4242749885 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m4146956986_gshared (List_1_t4242749885 * __this, CameraField_t2874564333  ___item0, const MethodInfo* method)
{
	{
		CameraFieldU5BU5D_t3664195264* L_0 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		CameraField_t2874564333  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CameraFieldU5BU5D_t3664195264*, CameraField_t2874564333 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (CameraFieldU5BU5D_t3664195264*)L_0, (CameraField_t2874564333 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3789090501_gshared (List_1_t4242749885 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		CameraFieldU5BU5D_t3664195264* L_5 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		int32_t L_6 = ___start0;
		CameraFieldU5BU5D_t3664195264* L_7 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		CameraFieldU5BU5D_t3664195264* L_15 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m3150005566_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m3150005566_gshared (List_1_t4242749885 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m3150005566_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1214267493_gshared (List_1_t4242749885 * __this, int32_t ___index0, CameraField_t2874564333  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t4242749885 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		CameraFieldU5BU5D_t3664195264* L_2 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t4242749885 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t4242749885 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		CameraFieldU5BU5D_t3664195264* L_4 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CameraField_t2874564333  L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CameraField_t2874564333 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m316889370_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m316889370_gshared (List_1_t4242749885 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m316889370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::Remove(T)
extern "C"  bool List_1_Remove_m629715961_gshared (List_1_t4242749885 * __this, CameraField_t2874564333  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		CameraField_t2874564333  L_0 = ___item0;
		NullCheck((List_1_t4242749885 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t4242749885 *, CameraField_t2874564333 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t4242749885 *)__this, (CameraField_t2874564333 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t4242749885 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m3383087659_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m3383087659_gshared (List_1_t4242749885 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m3383087659_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t4242749885 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		CameraFieldU5BU5D_t3664195264* L_5 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::ToArray()
extern "C"  CameraFieldU5BU5D_t3664195264* List_1_ToArray_m3415799290_gshared (List_1_t4242749885 * __this, const MethodInfo* method)
{
	CameraFieldU5BU5D_t3664195264* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (CameraFieldU5BU5D_t3664195264*)((CameraFieldU5BU5D_t3664195264*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		CameraFieldU5BU5D_t3664195264* L_1 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		CameraFieldU5BU5D_t3664195264* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		CameraFieldU5BU5D_t3664195264* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m464885226_gshared (List_1_t4242749885 * __this, const MethodInfo* method)
{
	{
		CameraFieldU5BU5D_t3664195264* L_0 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m2059264331_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m2059264331_gshared (List_1_t4242749885 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m2059264331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		CameraFieldU5BU5D_t3664195264** L_3 = (CameraFieldU5BU5D_t3664195264**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, CameraFieldU5BU5D_t3664195264**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)(NULL /*static, unused*/, (CameraFieldU5BU5D_t3664195264**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::get_Count()
extern "C"  int32_t List_1_get_Count_m3818922497_gshared (List_1_t4242749885 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m3621115345_MetadataUsageId;
extern "C"  CameraField_t2874564333  List_1_get_Item_m3621115345_gshared (List_1_t4242749885 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m3621115345_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		CameraFieldU5BU5D_t3664195264* L_3 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CameraField_t2874564333  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m489552892_MetadataUsageId;
extern "C"  void List_1_set_Item_m489552892_gshared (List_1_t4242749885 * __this, int32_t ___index0, CameraField_t2874564333  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m489552892_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t4242749885 *)__this);
		((  void (*) (List_1_t4242749885 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t4242749885 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		CameraFieldU5BU5D_t3664195264* L_4 = (CameraFieldU5BU5D_t3664195264*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CameraField_t2874564333  L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CameraField_t2874564333 )L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C"  void List_1__ctor_m3024762476_gshared (List_1_t1722560608 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		PIXEL_FORMATU5BU5D_t882070065* L_0 = ((List_1_t1722560608_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m733502843_gshared (List_1_t1722560608 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t1722560608 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		PIXEL_FORMATU5BU5D_t882070065* L_3 = ((List_1_t1722560608_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t1722560608 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((PIXEL_FORMATU5BU5D_t882070065*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t1722560608 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m3131339733_MetadataUsageId;
extern "C"  void List_1__ctor_m3131339733_gshared (List_1_t1722560608 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m3131339733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((PIXEL_FORMATU5BU5D_t882070065*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.cctor()
extern "C"  void List_1__cctor_m3312646057_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t1722560608_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((PIXEL_FORMATU5BU5D_t882070065*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2867103886_gshared (List_1_t1722560608 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t1722560608 *)__this);
		Enumerator_t1742233378  L_0 = ((  Enumerator_t1742233378  (*) (List_1_t1722560608 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1722560608 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t1742233378  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m800597312_gshared (List_1_t1722560608 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		PIXEL_FORMATU5BU5D_t882070065* L_0 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3640704207_gshared (List_1_t1722560608 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t1722560608 *)__this);
		Enumerator_t1742233378  L_0 = ((  Enumerator_t1742233378  (*) (List_1_t1722560608 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1722560608 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t1742233378  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m3003977806_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m3003977806_gshared (List_1_t1722560608 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m3003977806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1722560608 *)__this);
			((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m2773269298_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m2773269298_gshared (List_1_t1722560608 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m2773269298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1722560608 *)__this);
			bool L_1 = ((  bool (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m4186992550_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m4186992550_gshared (List_1_t1722560608 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m4186992550_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1722560608 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m579639065_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m579639065_gshared (List_1_t1722560608 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m579639065_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t1722560608 *)__this);
			((  void (*) (List_1_t1722560608 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)L_1, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m3733093743_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m3733093743_gshared (List_1_t1722560608 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m3733093743_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1722560608 *)__this);
			((  bool (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m959829171_gshared (List_1_t1722560608 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2249081116_gshared (List_1_t1722560608 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2064014243_gshared (List_1_t1722560608 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1722560608 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m3596312752_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m3596312752_gshared (List_1_t1722560608 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m3596312752_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t1722560608 *)__this);
			((  void (*) (List_1_t1722560608 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)L_0, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Add(T)
extern "C"  void List_1_Add_m2719123292_gshared (List_1_t1722560608 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		PIXEL_FORMATU5BU5D_t882070065* L_1 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		PIXEL_FORMATU5BU5D_t882070065* L_2 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		int32_t L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2512297526_gshared (List_1_t1722560608 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		PIXEL_FORMATU5BU5D_t882070065* L_3 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t1722560608 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t1722560608 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t1722560608 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m4231568692_gshared (List_1_t1722560608 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		PIXEL_FORMATU5BU5D_t882070065* L_5 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< PIXEL_FORMATU5BU5D_t882070065*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (PIXEL_FORMATU5BU5D_t882070065*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m3492540916_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m3492540916_gshared (List_1_t1722560608 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m3492540916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject* V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.Image/PIXEL_FORMAT>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (int32_t)L_3;
			int32_t L_4 = V_0;
			NullCheck((List_1_t1722560608 *)__this);
			((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3040515107_gshared (List_1_t1722560608 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t1722560608 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t1722560608 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t1722560608 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Clear()
extern "C"  void List_1_Clear_m853680303_gshared (List_1_t1722560608 * __this, const MethodInfo* method)
{
	{
		PIXEL_FORMATU5BU5D_t882070065* L_0 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		PIXEL_FORMATU5BU5D_t882070065* L_1 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(T)
extern "C"  bool List_1_Contains_m3353410806_gshared (List_1_t1722560608 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		PIXEL_FORMATU5BU5D_t882070065* L_0 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		int32_t L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, PIXEL_FORMATU5BU5D_t882070065*, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (PIXEL_FORMATU5BU5D_t882070065*)L_0, (int32_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2779978411_gshared (List_1_t1722560608 * __this, PIXEL_FORMATU5BU5D_t882070065* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		PIXEL_FORMATU5BU5D_t882070065* L_0 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		PIXEL_FORMATU5BU5D_t882070065* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetEnumerator()
extern "C"  Enumerator_t1742233378  List_1_GetEnumerator_m3833138014_gshared (List_1_t1722560608 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1742233378  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m704684707(&L_0, (List_1_t1722560608 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3810515127_gshared (List_1_t1722560608 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		PIXEL_FORMATU5BU5D_t882070065* L_0 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		int32_t L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, PIXEL_FORMATU5BU5D_t882070065*, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (PIXEL_FORMATU5BU5D_t882070065*)L_0, (int32_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2886145538_gshared (List_1_t1722560608 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		PIXEL_FORMATU5BU5D_t882070065* L_5 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		int32_t L_6 = ___start0;
		PIXEL_FORMATU5BU5D_t882070065* L_7 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		PIXEL_FORMATU5BU5D_t882070065* L_15 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m2526345595_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m2526345595_gshared (List_1_t1722560608 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m2526345595_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m471719906_gshared (List_1_t1722560608 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		PIXEL_FORMATU5BU5D_t882070065* L_2 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		PIXEL_FORMATU5BU5D_t882070065* L_4 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		int32_t L_5 = ___index0;
		int32_t L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m4025119447_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m4025119447_gshared (List_1_t1722560608 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m4025119447_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Remove(T)
extern "C"  bool List_1_Remove_m1031399963_gshared (List_1_t1722560608 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___item0;
		NullCheck((List_1_t1722560608 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m2640540072_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m2640540072_gshared (List_1_t1722560608 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m2640540072_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		PIXEL_FORMATU5BU5D_t882070065* L_5 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::ToArray()
extern "C"  PIXEL_FORMATU5BU5D_t882070065* List_1_ToArray_m3260422429_gshared (List_1_t1722560608 * __this, const MethodInfo* method)
{
	PIXEL_FORMATU5BU5D_t882070065* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (PIXEL_FORMATU5BU5D_t882070065*)((PIXEL_FORMATU5BU5D_t882070065*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		PIXEL_FORMATU5BU5D_t882070065* L_1 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		PIXEL_FORMATU5BU5D_t882070065* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		PIXEL_FORMATU5BU5D_t882070065* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m161914215_gshared (List_1_t1722560608 * __this, const MethodInfo* method)
{
	{
		PIXEL_FORMATU5BU5D_t882070065* L_0 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m4017453640_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m4017453640_gshared (List_1_t1722560608 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m4017453640_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		PIXEL_FORMATU5BU5D_t882070065** L_3 = (PIXEL_FORMATU5BU5D_t882070065**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, PIXEL_FORMATU5BU5D_t882070065**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)(NULL /*static, unused*/, (PIXEL_FORMATU5BU5D_t882070065**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Count()
extern "C"  int32_t List_1_get_Count_m1979159460_gshared (List_1_t1722560608 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m2219422222_MetadataUsageId;
extern "C"  int32_t List_1_get_Item_m2219422222_gshared (List_1_t1722560608 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m2219422222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		PIXEL_FORMATU5BU5D_t882070065* L_3 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m4160860217_MetadataUsageId;
extern "C"  void List_1_set_Item_m4160860217_gshared (List_1_t1722560608 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m4160860217_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1722560608 *)__this);
		((  void (*) (List_1_t1722560608 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1722560608 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		PIXEL_FORMATU5BU5D_t882070065* L_4 = (PIXEL_FORMATU5BU5D_t882070065*)__this->get__items_1();
		int32_t L_5 = ___index0;
		int32_t L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C"  void List_1__ctor_m1414933584_gshared (List_1_t682628370 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		TargetSearchResultU5BU5D_t1920244343* L_0 = ((List_1_t682628370_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3957273671_gshared (List_1_t682628370 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t682628370 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		TargetSearchResultU5BU5D_t1920244343* L_3 = ((List_1_t682628370_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t682628370 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((TargetSearchResultU5BU5D_t1920244343*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t682628370 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m2003846793_MetadataUsageId;
extern "C"  void List_1__ctor_m2003846793_gshared (List_1_t682628370 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m2003846793_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((TargetSearchResultU5BU5D_t1920244343*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.cctor()
extern "C"  void List_1__cctor_m2001718645_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t682628370_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((TargetSearchResultU5BU5D_t1920244343*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2347584394_gshared (List_1_t682628370 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t682628370 *)__this);
		Enumerator_t702301140  L_0 = ((  Enumerator_t702301140  (*) (List_1_t682628370 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t682628370 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t702301140  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3137939212_gshared (List_1_t682628370 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		TargetSearchResultU5BU5D_t1920244343* L_0 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1153382279_gshared (List_1_t682628370 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t682628370 *)__this);
		Enumerator_t702301140  L_0 = ((  Enumerator_t702301140  (*) (List_1_t682628370 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t682628370 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t702301140  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m3897426506_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m3897426506_gshared (List_1_t682628370 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m3897426506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t682628370 *)__this);
			((  void (*) (List_1_t682628370 *, TargetSearchResult_t3609410114 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t682628370 *)__this, (TargetSearchResult_t3609410114 )((*(TargetSearchResult_t3609410114 *)((TargetSearchResult_t3609410114 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m263376962_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m263376962_gshared (List_1_t682628370 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m263376962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t682628370 *)__this);
			bool L_1 = ((  bool (*) (List_1_t682628370 *, TargetSearchResult_t3609410114 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t682628370 *)__this, (TargetSearchResult_t3609410114 )((*(TargetSearchResult_t3609410114 *)((TargetSearchResult_t3609410114 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m3771728802_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3771728802_gshared (List_1_t682628370 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m3771728802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t682628370 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t682628370 *, TargetSearchResult_t3609410114 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t682628370 *)__this, (TargetSearchResult_t3609410114 )((*(TargetSearchResult_t3609410114 *)((TargetSearchResult_t3609410114 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m1010607565_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m1010607565_gshared (List_1_t682628370 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m1010607565_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t682628370 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t682628370 *)__this);
			((  void (*) (List_1_t682628370 *, int32_t, TargetSearchResult_t3609410114 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t682628370 *)__this, (int32_t)L_1, (TargetSearchResult_t3609410114 )((*(TargetSearchResult_t3609410114 *)((TargetSearchResult_t3609410114 *)UnBox (L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m1172085307_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m1172085307_gshared (List_1_t682628370 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m1172085307_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t682628370 *)__this);
			((  bool (*) (List_1_t682628370 *, TargetSearchResult_t3609410114 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t682628370 *)__this, (TargetSearchResult_t3609410114 )((*(TargetSearchResult_t3609410114 *)((TargetSearchResult_t3609410114 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1829819843_gshared (List_1_t682628370 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2957236998_gshared (List_1_t682628370 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3996984077_gshared (List_1_t682628370 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t682628370 *)__this);
		TargetSearchResult_t3609410114  L_1 = ((  TargetSearchResult_t3609410114  (*) (List_1_t682628370 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t682628370 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		TargetSearchResult_t3609410114  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m1145213540_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m1145213540_gshared (List_1_t682628370 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m1145213540_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t682628370 *)__this);
			((  void (*) (List_1_t682628370 *, int32_t, TargetSearchResult_t3609410114 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t682628370 *)__this, (int32_t)L_0, (TargetSearchResult_t3609410114 )((*(TargetSearchResult_t3609410114 *)((TargetSearchResult_t3609410114 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T)
extern "C"  void List_1_Add_m1109294400_gshared (List_1_t682628370 * __this, TargetSearchResult_t3609410114  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		TargetSearchResultU5BU5D_t1920244343* L_1 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t682628370 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		TargetSearchResultU5BU5D_t1920244343* L_2 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		TargetSearchResult_t3609410114  L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (TargetSearchResult_t3609410114 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2947946754_gshared (List_1_t682628370 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		TargetSearchResultU5BU5D_t1920244343* L_3 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t682628370 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t682628370 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t682628370 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t682628370 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2444355584_gshared (List_1_t682628370 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t682628370 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		TargetSearchResultU5BU5D_t1920244343* L_5 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< TargetSearchResultU5BU5D_t1920244343*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (TargetSearchResultU5BU5D_t1920244343*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m1705327808_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m1705327808_gshared (List_1_t682628370 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m1705327808_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TargetSearchResult_t3609410114  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			TargetSearchResult_t3609410114  L_3 = InterfaceFuncInvoker0< TargetSearchResult_t3609410114  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (TargetSearchResult_t3609410114 )L_3;
			TargetSearchResult_t3609410114  L_4 = V_0;
			NullCheck((List_1_t682628370 *)__this);
			((  void (*) (List_1_t682628370 *, TargetSearchResult_t3609410114 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t682628370 *)__this, (TargetSearchResult_t3609410114 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2633546199_gshared (List_1_t682628370 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t682628370 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t682628370 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t682628370 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Clear()
extern "C"  void List_1_Clear_m1919770979_gshared (List_1_t682628370 * __this, const MethodInfo* method)
{
	{
		TargetSearchResultU5BU5D_t1920244343* L_0 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		TargetSearchResultU5BU5D_t1920244343* L_1 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T)
extern "C"  bool List_1_Contains_m2557723537_gshared (List_1_t682628370 * __this, TargetSearchResult_t3609410114  ___item0, const MethodInfo* method)
{
	{
		TargetSearchResultU5BU5D_t1920244343* L_0 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		TargetSearchResult_t3609410114  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, TargetSearchResultU5BU5D_t1920244343*, TargetSearchResult_t3609410114 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (TargetSearchResultU5BU5D_t1920244343*)L_0, (TargetSearchResult_t3609410114 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3164788855_gshared (List_1_t682628370 * __this, TargetSearchResultU5BU5D_t1920244343* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		TargetSearchResultU5BU5D_t1920244343* L_0 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		TargetSearchResultU5BU5D_t1920244343* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator()
extern "C"  Enumerator_t702301140  List_1_GetEnumerator_m1131907150_gshared (List_1_t682628370 * __this, const MethodInfo* method)
{
	{
		Enumerator_t702301140  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m3526812969(&L_0, (List_1_t682628370 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2436220731_gshared (List_1_t682628370 * __this, TargetSearchResult_t3609410114  ___item0, const MethodInfo* method)
{
	{
		TargetSearchResultU5BU5D_t1920244343* L_0 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		TargetSearchResult_t3609410114  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, TargetSearchResultU5BU5D_t1920244343*, TargetSearchResult_t3609410114 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (TargetSearchResultU5BU5D_t1920244343*)L_0, (TargetSearchResult_t3609410114 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m275924942_gshared (List_1_t682628370 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		TargetSearchResultU5BU5D_t1920244343* L_5 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		int32_t L_6 = ___start0;
		TargetSearchResultU5BU5D_t1920244343* L_7 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		TargetSearchResultU5BU5D_t1920244343* L_15 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m2911156039_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m2911156039_gshared (List_1_t682628370 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m2911156039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2268766382_gshared (List_1_t682628370 * __this, int32_t ___index0, TargetSearchResult_t3609410114  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t682628370 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		TargetSearchResultU5BU5D_t1920244343* L_2 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t682628370 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t682628370 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		TargetSearchResultU5BU5D_t1920244343* L_4 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		int32_t L_5 = ___index0;
		TargetSearchResult_t3609410114  L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (TargetSearchResult_t3609410114 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m205273763_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m205273763_gshared (List_1_t682628370 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m205273763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Remove(T)
extern "C"  bool List_1_Remove_m2889233356_gshared (List_1_t682628370 * __this, TargetSearchResult_t3609410114  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TargetSearchResult_t3609410114  L_0 = ___item0;
		NullCheck((List_1_t682628370 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t682628370 *, TargetSearchResult_t3609410114 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t682628370 *)__this, (TargetSearchResult_t3609410114 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t682628370 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m142619252_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m142619252_gshared (List_1_t682628370 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m142619252_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t682628370 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		TargetSearchResultU5BU5D_t1920244343* L_5 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::ToArray()
extern "C"  TargetSearchResultU5BU5D_t1920244343* List_1_ToArray_m302001847_gshared (List_1_t682628370 * __this, const MethodInfo* method)
{
	TargetSearchResultU5BU5D_t1920244343* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (TargetSearchResultU5BU5D_t1920244343*)((TargetSearchResultU5BU5D_t1920244343*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		TargetSearchResultU5BU5D_t1920244343* L_1 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		TargetSearchResultU5BU5D_t1920244343* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		TargetSearchResultU5BU5D_t1920244343* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3327764971_gshared (List_1_t682628370 * __this, const MethodInfo* method)
{
	{
		TargetSearchResultU5BU5D_t1920244343* L_0 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m158135572_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m158135572_gshared (List_1_t682628370 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m158135572_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		TargetSearchResultU5BU5D_t1920244343** L_3 = (TargetSearchResultU5BU5D_t1920244343**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, TargetSearchResultU5BU5D_t1920244343**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)(NULL /*static, unused*/, (TargetSearchResultU5BU5D_t1920244343**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count()
extern "C"  int32_t List_1_get_Count_m2325706144_gshared (List_1_t682628370 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m2001746744_MetadataUsageId;
extern "C"  TargetSearchResult_t3609410114  List_1_get_Item_m2001746744_gshared (List_1_t682628370 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m2001746744_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		TargetSearchResultU5BU5D_t1920244343* L_3 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		TargetSearchResult_t3609410114  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m250703365_MetadataUsageId;
extern "C"  void List_1_set_Item_m250703365_gshared (List_1_t682628370 * __this, int32_t ___index0, TargetSearchResult_t3609410114  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m250703365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t682628370 *)__this);
		((  void (*) (List_1_t682628370 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t682628370 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		TargetSearchResultU5BU5D_t1920244343* L_4 = (TargetSearchResultU5BU5D_t1920244343*)__this->get__items_1();
		int32_t L_5 = ___index0;
		TargetSearchResult_t3609410114  L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (TargetSearchResult_t3609410114 )L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::.ctor()
extern "C"  void List_1__ctor_m1515543171_gshared (List_1_t595805021 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		IdPairU5BU5D_t296135328* L_0 = ((List_1_t595805021_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3521223093_gshared (List_1_t595805021 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t595805021 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		IdPairU5BU5D_t296135328* L_3 = ((List_1_t595805021_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t595805021 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VuforiaManagerImpl/IdPair>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((IdPairU5BU5D_t296135328*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t595805021 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m3065537364_MetadataUsageId;
extern "C"  void List_1__ctor_m3065537364_gshared (List_1_t595805021 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m3065537364_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((IdPairU5BU5D_t296135328*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::.cctor()
extern "C"  void List_1__cctor_m3550069130_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t595805021_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((IdPairU5BU5D_t296135328*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2901349141_gshared (List_1_t595805021 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t595805021 *)__this);
		Enumerator_t615477791  L_0 = ((  Enumerator_t615477791  (*) (List_1_t595805021 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t595805021 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t615477791  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3591100449_gshared (List_1_t595805021 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		IdPairU5BU5D_t296135328* L_0 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1068006492_gshared (List_1_t595805021 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t595805021 *)__this);
		Enumerator_t615477791  L_0 = ((  Enumerator_t615477791  (*) (List_1_t595805021 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t595805021 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t615477791  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m3528411413_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m3528411413_gshared (List_1_t595805021 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m3528411413_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t595805021 *)__this);
			((  void (*) (List_1_t595805021 *, IdPair_t3522586765 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t595805021 *)__this, (IdPair_t3522586765 )((*(IdPair_t3522586765 *)((IdPair_t3522586765 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m350287063_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m350287063_gshared (List_1_t595805021 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m350287063_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t595805021 *)__this);
			bool L_1 = ((  bool (*) (List_1_t595805021 *, IdPair_t3522586765 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t595805021 *)__this, (IdPair_t3522586765 )((*(IdPair_t3522586765 *)((IdPair_t3522586765 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m3354062061_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3354062061_gshared (List_1_t595805021 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m3354062061_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t595805021 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t595805021 *, IdPair_t3522586765 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t595805021 *)__this, (IdPair_t3522586765 )((*(IdPair_t3522586765 *)((IdPair_t3522586765 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m2604325784_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m2604325784_gshared (List_1_t595805021 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m2604325784_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t595805021 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t595805021 *)__this);
			((  void (*) (List_1_t595805021 *, int32_t, IdPair_t3522586765 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t595805021 *)__this, (int32_t)L_1, (IdPair_t3522586765 )((*(IdPair_t3522586765 *)((IdPair_t3522586765 *)UnBox (L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m2351158928_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m2351158928_gshared (List_1_t595805021 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m2351158928_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t595805021 *)__this);
			((  bool (*) (List_1_t595805021 *, IdPair_t3522586765 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t595805021 *)__this, (IdPair_t3522586765 )((*(IdPair_t3522586765 *)((IdPair_t3522586765 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m587072536_gshared (List_1_t595805021 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3224091729_gshared (List_1_t595805021 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2746342808_gshared (List_1_t595805021 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t595805021 *)__this);
		IdPair_t3522586765  L_1 = ((  IdPair_t3522586765  (*) (List_1_t595805021 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t595805021 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		IdPair_t3522586765  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t1441619295_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t241699085_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m3700064623_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m3700064623_gshared (List_1_t595805021 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m3700064623_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t595805021 *)__this);
			((  void (*) (List_1_t595805021 *, int32_t, IdPair_t3522586765 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t595805021 *)__this, (int32_t)L_0, (IdPair_t3522586765 )((*(IdPair_t3522586765 *)((IdPair_t3522586765 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t1441619295_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t241699085_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::Add(T)
extern "C"  void List_1_Add_m2671430044_gshared (List_1_t595805021 * __this, IdPair_t3522586765  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		IdPairU5BU5D_t296135328* L_1 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t595805021 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		IdPairU5BU5D_t296135328* L_2 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		IdPair_t3522586765  L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (IdPair_t3522586765 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m803541463_gshared (List_1_t595805021 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		IdPairU5BU5D_t296135328* L_3 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t595805021 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t595805021 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t595805021 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t595805021 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2357218517_gshared (List_1_t595805021 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VuforiaManagerImpl/IdPair>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t595805021 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		IdPairU5BU5D_t296135328* L_5 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< IdPairU5BU5D_t296135328*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<Vuforia.VuforiaManagerImpl/IdPair>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (IdPairU5BU5D_t296135328*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m1618190741_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m1618190741_gshared (List_1_t595805021 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m1618190741_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IdPair_t3522586765  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VuforiaManagerImpl/IdPair>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			IdPair_t3522586765  L_3 = InterfaceFuncInvoker0< IdPair_t3522586765  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<Vuforia.VuforiaManagerImpl/IdPair>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (IdPair_t3522586765 )L_3;
			IdPair_t3522586765  L_4 = V_0;
			NullCheck((List_1_t595805021 *)__this);
			((  void (*) (List_1_t595805021 *, IdPair_t3522586765 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t595805021 *)__this, (IdPair_t3522586765 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1293542370_gshared (List_1_t595805021 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t595805021 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t595805021 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t595805021 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::Clear()
extern "C"  void List_1_Clear_m3216643758_gshared (List_1_t595805021 * __this, const MethodInfo* method)
{
	{
		IdPairU5BU5D_t296135328* L_0 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		IdPairU5BU5D_t296135328* L_1 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::Contains(T)
extern "C"  bool List_1_Contains_m823149020_gshared (List_1_t595805021 * __this, IdPair_t3522586765  ___item0, const MethodInfo* method)
{
	{
		IdPairU5BU5D_t296135328* L_0 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		IdPair_t3522586765  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IdPairU5BU5D_t296135328*, IdPair_t3522586765 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (IdPairU5BU5D_t296135328*)L_0, (IdPair_t3522586765 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3967025804_gshared (List_1_t595805021 * __this, IdPairU5BU5D_t296135328* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		IdPairU5BU5D_t296135328* L_0 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		IdPairU5BU5D_t296135328* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::GetEnumerator()
extern "C"  Enumerator_t615477791  List_1_GetEnumerator_m1885753824_gshared (List_1_t595805021 * __this, const MethodInfo* method)
{
	{
		Enumerator_t615477791  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m927528884(&L_0, (List_1_t595805021 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3627519760_gshared (List_1_t595805021 * __this, IdPair_t3522586765  ___item0, const MethodInfo* method)
{
	{
		IdPairU5BU5D_t296135328* L_0 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		IdPair_t3522586765  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IdPairU5BU5D_t296135328*, IdPair_t3522586765 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (IdPairU5BU5D_t296135328*)L_0, (IdPair_t3522586765 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1125551331_gshared (List_1_t595805021 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		IdPairU5BU5D_t296135328* L_5 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		int32_t L_6 = ___start0;
		IdPairU5BU5D_t296135328* L_7 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		IdPairU5BU5D_t296135328* L_15 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m3713392988_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m3713392988_gshared (List_1_t595805021 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m3713392988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2953399299_gshared (List_1_t595805021 * __this, int32_t ___index0, IdPair_t3522586765  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t595805021 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		IdPairU5BU5D_t296135328* L_2 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t595805021 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t595805021 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		IdPairU5BU5D_t296135328* L_4 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		int32_t L_5 = ___index0;
		IdPair_t3522586765  L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (IdPair_t3522586765 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m2365898296_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m2365898296_gshared (List_1_t595805021 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m2365898296_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::Remove(T)
extern "C"  bool List_1_Remove_m4192454871_gshared (List_1_t595805021 * __this, IdPair_t3522586765  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IdPair_t3522586765  L_0 = ___item0;
		NullCheck((List_1_t595805021 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t595805021 *, IdPair_t3522586765 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t595805021 *)__this, (IdPair_t3522586765 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t595805021 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m827252169_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m827252169_gshared (List_1_t595805021 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m827252169_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t595805021 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		IdPairU5BU5D_t296135328* L_5 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::ToArray()
extern "C"  IdPairU5BU5D_t296135328* List_1_ToArray_m3391953794_gshared (List_1_t595805021 * __this, const MethodInfo* method)
{
	IdPairU5BU5D_t296135328* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (IdPairU5BU5D_t296135328*)((IdPairU5BU5D_t296135328*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		IdPairU5BU5D_t296135328* L_1 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		IdPairU5BU5D_t296135328* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		IdPairU5BU5D_t296135328* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2765717312_gshared (List_1_t595805021 * __this, const MethodInfo* method)
{
	{
		IdPairU5BU5D_t296135328* L_0 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m2308697577_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m2308697577_gshared (List_1_t595805021 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m2308697577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		IdPairU5BU5D_t296135328** L_3 = (IdPairU5BU5D_t296135328**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, IdPairU5BU5D_t296135328**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)(NULL /*static, unused*/, (IdPairU5BU5D_t296135328**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::get_Count()
extern "C"  int32_t List_1_get_Count_m601270379_gshared (List_1_t595805021 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m1578029517_MetadataUsageId;
extern "C"  IdPair_t3522586765  List_1_get_Item_m1578029517_gshared (List_1_t595805021 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m1578029517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		IdPairU5BU5D_t296135328* L_3 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		IdPair_t3522586765  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.VuforiaManagerImpl/IdPair>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m1052940314_MetadataUsageId;
extern "C"  void List_1_set_Item_m1052940314_gshared (List_1_t595805021 * __this, int32_t ___index0, IdPair_t3522586765  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m1052940314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t595805021 *)__this);
		((  void (*) (List_1_t595805021 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t595805021 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		IdPairU5BU5D_t296135328* L_4 = (IdPairU5BU5D_t296135328*)__this->get__items_1();
		int32_t L_5 = ___index0;
		IdPair_t3522586765  L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (IdPair_t3522586765 )L_6);
		return;
	}
}
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C"  void Enumerator__ctor_m1003414509_gshared (Enumerator_t2532196025 * __this, Stack_1_t2974409999 * ___t0, const MethodInfo* method)
{
	{
		Stack_1_t2974409999 * L_0 = ___t0;
		__this->set_parent_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		Stack_1_t2974409999 * L_1 = ___t0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_2();
		__this->set__version_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1003414509_AdjustorThunk (Il2CppObject * __this, Stack_1_t2974409999 * ___t0, const MethodInfo* method)
{
	Enumerator_t2532196025 * _thisAdjusted = reinterpret_cast<Enumerator_t2532196025 *>(__this + 1);
	Enumerator__ctor_m1003414509(_thisAdjusted, ___t0, method);
}
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m3054975473_MetadataUsageId;
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3054975473_gshared (Enumerator_t2532196025 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m3054975473_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get__version_2();
		Stack_1_t2974409999 * L_1 = (Stack_1_t2974409999 *)__this->get_parent_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_2();
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3054975473_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2532196025 * _thisAdjusted = reinterpret_cast<Enumerator_t2532196025 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3054975473(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2470832103_gshared (Enumerator_t2532196025 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t2532196025 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2470832103_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2532196025 * _thisAdjusted = reinterpret_cast<Enumerator_t2532196025 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2470832103(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1634653158_gshared (Enumerator_t2532196025 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1634653158_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2532196025 * _thisAdjusted = reinterpret_cast<Enumerator_t2532196025 *>(__this + 1);
	Enumerator_Dispose_m1634653158(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_MoveNext_m3012756789_MetadataUsageId;
extern "C"  bool Enumerator_MoveNext_m3012756789_gshared (Enumerator_t2532196025 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m3012756789_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__version_2();
		Stack_1_t2974409999 * L_1 = (Stack_1_t2974409999 *)__this->get_parent_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_2();
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_003a;
		}
	}
	{
		Stack_1_t2974409999 * L_5 = (Stack_1_t2974409999 *)__this->get_parent_0();
		NullCheck(L_5);
		int32_t L_6 = (int32_t)L_5->get__size_1();
		__this->set_idx_1(L_6);
	}

IL_003a:
	{
		int32_t L_7 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_7) == ((int32_t)(-1))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_8 = (int32_t)__this->get_idx_1();
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8-(int32_t)1));
		V_0 = (int32_t)L_9;
		__this->set_idx_1(L_9);
		int32_t L_10 = V_0;
		G_B7_0 = ((((int32_t)((((int32_t)L_10) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0060;
	}

IL_005f:
	{
		G_B7_0 = 0;
	}

IL_0060:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3012756789_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2532196025 * _thisAdjusted = reinterpret_cast<Enumerator_t2532196025 *>(__this + 1);
	return Enumerator_MoveNext_m3012756789(_thisAdjusted, method);
}
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_get_Current_m2483819640_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_get_Current_m2483819640_gshared (Enumerator_t2532196025 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m2483819640_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Stack_1_t2974409999 * L_2 = (Stack_1_t2974409999 *)__this->get_parent_0();
		NullCheck(L_2);
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)L_2->get__array_0();
		int32_t L_4 = (int32_t)__this->get_idx_1();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m2483819640_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2532196025 * _thisAdjusted = reinterpret_cast<Enumerator_t2532196025 *>(__this + 1);
	return Enumerator_get_Current_m2483819640(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C"  void Stack_1__ctor_m2725689112_gshared (Stack_1_t2974409999 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Stack_1_System_Collections_ICollection_get_SyncRoot_m2938343088_gshared (Stack_1_t2974409999 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Void System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ArrayTypeMismatchException_t3681076194_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern const uint32_t Stack_1_System_Collections_ICollection_CopyTo_m3277353260_MetadataUsageId;
extern "C"  void Stack_1_System_Collections_ICollection_CopyTo_m3277353260_gshared (Stack_1_t2974409999 * __this, Il2CppArray * ___dest0, int32_t ___idx1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stack_1_System_Collections_ICollection_CopyTo_m3277353260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get__array_0();
			if (!L_0)
			{
				goto IL_0025;
			}
		}

IL_000b:
		{
			ObjectU5BU5D_t1108656482* L_1 = (ObjectU5BU5D_t1108656482*)__this->get__array_0();
			Il2CppArray * L_2 = ___dest0;
			int32_t L_3 = ___idx1;
			NullCheck((Il2CppArray *)(Il2CppArray *)L_1);
			Array_CopyTo_m910233845((Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
			Il2CppArray * L_4 = ___dest0;
			int32_t L_5 = ___idx1;
			int32_t L_6 = (int32_t)__this->get__size_1();
			Array_Reverse_m3064094494(NULL /*static, unused*/, (Il2CppArray *)L_4, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		}

IL_0025:
		{
			goto IL_0036;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArrayTypeMismatchException_t3681076194_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002a;
		throw e;
	}

CATCH_002a:
	{ // begin catch(System.ArrayTypeMismatchException)
		{
			ArgumentException_t928607144 * L_7 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m571182463(L_7, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0031:
		{
			goto IL_0036;
		}
	} // end catch (depth: 1)

IL_0036:
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m625377314_gshared (Stack_1_t2974409999 * __this, const MethodInfo* method)
{
	{
		NullCheck((Stack_1_t2974409999 *)__this);
		Enumerator_t2532196025  L_0 = ((  Enumerator_t2532196025  (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Stack_1_t2974409999 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Enumerator_t2532196025  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Stack_1_System_Collections_IEnumerable_GetEnumerator_m4095051687_gshared (Stack_1_t2974409999 * __this, const MethodInfo* method)
{
	{
		NullCheck((Stack_1_t2974409999 *)__this);
		Enumerator_t2532196025  L_0 = ((  Enumerator_t2532196025  (*) (Stack_1_t2974409999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Stack_1_t2974409999 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Enumerator_t2532196025  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// T System.Collections.Generic.Stack`1<System.Object>::Pop()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Stack_1_Pop_m4267009222_MetadataUsageId;
extern "C"  Il2CppObject * Stack_1_Pop_m4267009222_gshared (Stack_1_t2974409999 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stack_1_Pop_m4267009222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_1();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = (int32_t)__this->get__version_2();
		__this->set__version_2(((int32_t)((int32_t)L_2+(int32_t)1)));
		ObjectU5BU5D_t1108656482* L_3 = (ObjectU5BU5D_t1108656482*)__this->get__array_0();
		int32_t L_4 = (int32_t)__this->get__size_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_1 = (int32_t)L_5;
		__this->set__size_1(L_5);
		int32_t L_6 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_0 = (Il2CppObject *)L_8;
		ObjectU5BU5D_t1108656482* L_9 = (ObjectU5BU5D_t1108656482*)__this->get__array_0();
		int32_t L_10 = (int32_t)__this->get__size_1();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_11 = V_2;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Il2CppObject *)L_11);
		Il2CppObject * L_12 = V_0;
		return L_12;
	}
}
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(T)
extern "C"  void Stack_1_Push_m3350166104_gshared (Stack_1_t2974409999 * __this, Il2CppObject * ___t0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	ObjectU5BU5D_t1108656482** G_B4_0 = NULL;
	ObjectU5BU5D_t1108656482** G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t1108656482** G_B5_1 = NULL;
	{
		ObjectU5BU5D_t1108656482* L_0 = (ObjectU5BU5D_t1108656482*)__this->get__array_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get__size_1();
		ObjectU5BU5D_t1108656482* L_2 = (ObjectU5BU5D_t1108656482*)__this->get__array_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0043;
		}
	}

IL_001e:
	{
		ObjectU5BU5D_t1108656482** L_3 = (ObjectU5BU5D_t1108656482**)__this->get_address_of__array_0();
		int32_t L_4 = (int32_t)__this->get__size_1();
		G_B3_0 = L_3;
		if (L_4)
		{
			G_B4_0 = L_3;
			goto IL_0036;
		}
	}
	{
		G_B5_0 = ((int32_t)16);
		G_B5_1 = G_B3_0;
		goto IL_003e;
	}

IL_0036:
	{
		int32_t L_5 = (int32_t)__this->get__size_1();
		G_B5_0 = ((int32_t)((int32_t)2*(int32_t)L_5));
		G_B5_1 = G_B4_0;
	}

IL_003e:
	{
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t1108656482**)G_B5_1, (int32_t)G_B5_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
	}

IL_0043:
	{
		int32_t L_6 = (int32_t)__this->get__version_2();
		__this->set__version_2(((int32_t)((int32_t)L_6+(int32_t)1)));
		ObjectU5BU5D_t1108656482* L_7 = (ObjectU5BU5D_t1108656482*)__this->get__array_0();
		int32_t L_8 = (int32_t)__this->get__size_1();
		int32_t L_9 = (int32_t)L_8;
		V_0 = (int32_t)L_9;
		__this->set__size_1(((int32_t)((int32_t)L_9+(int32_t)1)));
		int32_t L_10 = V_0;
		Il2CppObject * L_11 = ___t0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Il2CppObject *)L_11);
		return;
	}
}
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m3631765324_gshared (Stack_1_t2974409999 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_1();
		return L_0;
	}
}
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2532196025  Stack_1_GetEnumerator_m202302354_gshared (Stack_1_t2974409999 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2532196025  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1003414509(&L_0, (Stack_1_t2974409999 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_0;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::.ctor()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1__ctor_m1690372513_MetadataUsageId;
extern "C"  void Collection_1__ctor_m1690372513_gshared (Collection_1_t3356274529 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1__ctor_m1690372513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1244034627 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t1244034627 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (List_1_t1244034627 *)L_0;
		List_1_t1244034627 * L_1 = V_0;
		V_1 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_1;
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(1 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_syncRoot_1(L_3);
		List_1_t1244034627 * L_4 = V_0;
		__this->set_list_0(L_4);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1624016570_gshared (Collection_1_t3356274529 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_System_Collections_ICollection_CopyTo_m1285013187_MetadataUsageId;
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1285013187_gshared (Collection_1_t3356274529 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_System_Collections_ICollection_CopyTo_m1285013187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t2643922881_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t2643922881_il2cpp_TypeInfo_var)), (Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2828471038_gshared (Collection_1_t3356274529 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1708617267_gshared (Collection_1_t3356274529 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Il2CppObject * L_3 = ___value0;
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t3356274529 *)__this);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T) */, (Collection_1_t3356274529 *)__this, (int32_t)L_2, (Il2CppObject *)L_4);
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m504494585_gshared (Collection_1_t3356274529 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_2, (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1652511499_gshared (Collection_1_t3356274529 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_2, (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2187188150_gshared (Collection_1_t3356274529 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t3356274529 *)__this);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T) */, (Collection_1_t3356274529 *)__this, (int32_t)L_0, (Il2CppObject *)L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2625864114_gshared (Collection_1_t3356274529 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		Il2CppObject * L_1 = ___value0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t3356274529 *)__this);
		int32_t L_3 = ((  int32_t (*) (Collection_1_t3356274529 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Collection_1_t3356274529 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		NullCheck((Collection_1_t3356274529 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32) */, (Collection_1_t3356274529 *)__this, (int32_t)L_4);
		return;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1873829551_gshared (Collection_1_t3356274529 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_1();
		return L_0;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2224513142_gshared (Collection_1_t3356274529 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2262756877_gshared (Collection_1_t3356274529 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t3356274529 *)__this);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T) */, (Collection_1_t3356274529 *)__this, (int32_t)L_0, (Il2CppObject *)L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(T)
extern "C"  void Collection_1_Add_m321765054_gshared (Collection_1_t3356274529 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Il2CppObject * L_3 = ___item0;
		NullCheck((Collection_1_t3356274529 *)__this);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T) */, (Collection_1_t3356274529 *)__this, (int32_t)L_2, (Il2CppObject *)L_3);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Clear()
extern "C"  void Collection_1_Clear_m3391473100_gshared (Collection_1_t3356274529 * __this, const MethodInfo* method)
{
	{
		NullCheck((Collection_1_t3356274529 *)__this);
		VirtActionInvoker0::Invoke(30 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::ClearItems() */, (Collection_1_t3356274529 *)__this);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2738199222_gshared (Collection_1_t3356274529 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Contains(T)
extern "C"  bool Collection_1_Contains_m1050871674_gshared (Collection_1_t3356274529 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1746187054_gshared (Collection_1_t3356274529 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		ObjectU5BU5D_t1108656482* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< ObjectU5BU5D_t1108656482*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (ObjectU5BU5D_t1108656482*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m625631581_gshared (Collection_1_t3356274529 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3101447730_gshared (Collection_1_t3356274529 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1208073509_gshared (Collection_1_t3356274529 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___item1;
		NullCheck((Collection_1_t3356274529 *)__this);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T) */, (Collection_1_t3356274529 *)__this, (int32_t)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m714854616_gshared (Collection_1_t3356274529 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		Il2CppObject * L_2 = ___item1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<System.Object>::Insert(System.Int32,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1, (Il2CppObject *)L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(T)
extern "C"  bool Collection_1_Remove_m2181520885_gshared (Collection_1_t3356274529 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((Collection_1_t3356274529 *)__this);
		int32_t L_1 = ((  int32_t (*) (Collection_1_t3356274529 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Collection_1_t3356274529 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		return (bool)0;
	}

IL_0011:
	{
		int32_t L_3 = V_0;
		NullCheck((Collection_1_t3356274529 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32) */, (Collection_1_t3356274529 *)__this, (int32_t)L_3);
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3376893675_gshared (Collection_1_t3356274529 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((Collection_1_t3356274529 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32) */, (Collection_1_t3356274529 *)__this, (int32_t)L_0);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1099170891_gshared (Collection_1_t3356274529 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Object>::RemoveAt(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		return;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1472906633_gshared (Collection_1_t3356274529 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_get_Item_m2356360623_gshared (Collection_1_t3356274529 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3127068860_gshared (Collection_1_t3356274529 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		NullCheck((Collection_1_t3356274529 *)__this);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T) */, (Collection_1_t3356274529 *)__this, (int32_t)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m112162877_gshared (Collection_1_t3356274529 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		Il2CppObject * L_2 = ___item1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Object>::set_Item(System.Int32,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1, (Il2CppObject *)L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsValidItem(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_IsValidItem_m1993492338_MetadataUsageId;
extern "C"  bool Collection_1_IsValidItem_m1993492338_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_IsValidItem_m1993492338_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___item0;
		if (((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))
		{
			goto IL_0028;
		}
	}
	{
		Il2CppObject * L_1 = ___item0;
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		bool L_3 = Type_get_IsValueType_m1914757235((Type_t *)L_2, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B4_0 = 0;
	}

IL_0026:
	{
		G_B6_0 = G_B4_0;
		goto IL_0029;
	}

IL_0028:
	{
		G_B6_0 = 1;
	}

IL_0029:
	{
		return (bool)G_B6_0;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Object>::ConvertItem(System.Object)
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t Collection_1_ConvertItem_m1655469326_MetadataUsageId;
extern "C"  Il2CppObject * Collection_1_ConvertItem_m1655469326_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_ConvertItem_m1655469326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___item0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_2 = ___item0;
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)));
	}

IL_0012:
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_CheckWritable_m651250670_MetadataUsageId;
extern "C"  void Collection_1_CheckWritable_m651250670_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_CheckWritable_m651250670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___list0;
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1732551818 * L_2 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1__ctor_m1235362998_MetadataUsageId;
extern "C"  void Collection_1__ctor_m1235362998_gshared (Collection_1_t2245071147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1__ctor_m1235362998_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t132831245 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		List_1_t132831245 * L_0 = (List_1_t132831245 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t132831245 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (List_1_t132831245 *)L_0;
		List_1_t132831245 * L_1 = V_0;
		V_1 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_1;
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(1 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_syncRoot_1(L_3);
		List_1_t132831245 * L_4 = V_0;
		__this->set_list_0(L_4);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m749697729_gshared (Collection_1_t2245071147 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_System_Collections_ICollection_CopyTo_m3417958734_MetadataUsageId;
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3417958734_gshared (Collection_1_t2245071147 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_System_Collections_ICollection_CopyTo_m3417958734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t2643922881_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t2643922881_il2cpp_TypeInfo_var)), (Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m934847197_gshared (Collection_1_t2245071147 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2420983424_gshared (Collection_1_t2245071147 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Il2CppObject * L_3 = ___value0;
		CustomAttributeNamedArgument_t3059612989  L_4 = ((  CustomAttributeNamedArgument_t3059612989  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t2245071147 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t3059612989  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2245071147 *)__this, (int32_t)L_2, (CustomAttributeNamedArgument_t3059612989 )L_4);
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3744332480_gshared (Collection_1_t2245071147 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, CustomAttributeNamedArgument_t3059612989  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_2, (CustomAttributeNamedArgument_t3059612989 )((*(CustomAttributeNamedArgument_t3059612989 *)((CustomAttributeNamedArgument_t3059612989 *)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m847658200_gshared (Collection_1_t2245071147 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t3059612989  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_2, (CustomAttributeNamedArgument_t3059612989 )((*(CustomAttributeNamedArgument_t3059612989 *)((CustomAttributeNamedArgument_t3059612989 *)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m793248331_gshared (Collection_1_t2245071147 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		CustomAttributeNamedArgument_t3059612989  L_2 = ((  CustomAttributeNamedArgument_t3059612989  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t2245071147 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t3059612989  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2245071147 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t3059612989 )L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m979290365_gshared (Collection_1_t2245071147 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		Il2CppObject * L_1 = ___value0;
		CustomAttributeNamedArgument_t3059612989  L_2 = ((  CustomAttributeNamedArgument_t3059612989  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t2245071147 *)__this);
		int32_t L_3 = ((  int32_t (*) (Collection_1_t2245071147 *, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Collection_1_t2245071147 *)__this, (CustomAttributeNamedArgument_t3059612989 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		NullCheck((Collection_1_t2245071147 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2245071147 *)__this, (int32_t)L_4);
		return;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m81883406_gshared (Collection_1_t2245071147 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_1();
		return L_0;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2446153493_gshared (Collection_1_t2245071147 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeNamedArgument_t3059612989  L_2 = InterfaceFuncInvoker1< CustomAttributeNamedArgument_t3059612989 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		CustomAttributeNamedArgument_t3059612989  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2716387170_gshared (Collection_1_t2245071147 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		CustomAttributeNamedArgument_t3059612989  L_2 = ((  CustomAttributeNamedArgument_t3059612989  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t2245071147 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t3059612989  >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::SetItem(System.Int32,T) */, (Collection_1_t2245071147 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t3059612989 )L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void Collection_1_Add_m1692560649_gshared (Collection_1_t2245071147 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		CustomAttributeNamedArgument_t3059612989  L_3 = ___item0;
		NullCheck((Collection_1_t2245071147 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t3059612989  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2245071147 *)__this, (int32_t)L_2, (CustomAttributeNamedArgument_t3059612989 )L_3);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void Collection_1_Clear_m2936463585_gshared (Collection_1_t2245071147 * __this, const MethodInfo* method)
{
	{
		NullCheck((Collection_1_t2245071147 *)__this);
		VirtActionInvoker0::Invoke(30 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ClearItems() */, (Collection_1_t2245071147 *)__this);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3651347649_gshared (Collection_1_t2245071147 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool Collection_1_Contains_m2554792531_gshared (Collection_1_t2245071147 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeNamedArgument_t3059612989  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, CustomAttributeNamedArgument_t3059612989  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (CustomAttributeNamedArgument_t3059612989 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1143243961_gshared (Collection_1_t2245071147 * __this, CustomAttributeNamedArgumentU5BU5D_t1983528240* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< CustomAttributeNamedArgumentU5BU5D_t1983528240*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3784377642_gshared (Collection_1_t2245071147 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2242432069_gshared (Collection_1_t2245071147 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeNamedArgument_t3059612989  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t3059612989  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (CustomAttributeNamedArgument_t3059612989 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3361633648_gshared (Collection_1_t2245071147 * __this, int32_t ___index0, CustomAttributeNamedArgument_t3059612989  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgument_t3059612989  L_1 = ___item1;
		NullCheck((Collection_1_t2245071147 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t3059612989  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2245071147 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t3059612989 )L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1107127203_gshared (Collection_1_t2245071147 * __this, int32_t ___index0, CustomAttributeNamedArgument_t3059612989  ___item1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		CustomAttributeNamedArgument_t3059612989  L_2 = ___item1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< int32_t, CustomAttributeNamedArgument_t3059612989  >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1, (CustomAttributeNamedArgument_t3059612989 )L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool Collection_1_Remove_m3394257678_gshared (Collection_1_t2245071147 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		CustomAttributeNamedArgument_t3059612989  L_0 = ___item0;
		NullCheck((Collection_1_t2245071147 *)__this);
		int32_t L_1 = ((  int32_t (*) (Collection_1_t2245071147 *, CustomAttributeNamedArgument_t3059612989 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Collection_1_t2245071147 *)__this, (CustomAttributeNamedArgument_t3059612989 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		return (bool)0;
	}

IL_0011:
	{
		int32_t L_3 = V_0;
		NullCheck((Collection_1_t2245071147 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2245071147 *)__this, (int32_t)L_3);
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1235486518_gshared (Collection_1_t2245071147 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((Collection_1_t2245071147 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2245071147 *)__this, (int32_t)L_0);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m496227798_gshared (Collection_1_t2245071147 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		return;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m613224918_gshared (Collection_1_t2245071147 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeNamedArgument_t3059612989  Collection_1_get_Item_m447264732_gshared (Collection_1_t2245071147 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeNamedArgument_t3059612989  L_2 = InterfaceFuncInvoker1< CustomAttributeNamedArgument_t3059612989 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2524125767_gshared (Collection_1_t2245071147 * __this, int32_t ___index0, CustomAttributeNamedArgument_t3059612989  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgument_t3059612989  L_1 = ___value1;
		NullCheck((Collection_1_t2245071147 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t3059612989  >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::SetItem(System.Int32,T) */, (Collection_1_t2245071147 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t3059612989 )L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2448017746_gshared (Collection_1_t2245071147 * __this, int32_t ___index0, CustomAttributeNamedArgument_t3059612989  ___item1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		CustomAttributeNamedArgument_t3059612989  L_2 = ___item1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< int32_t, CustomAttributeNamedArgument_t3059612989  >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1, (CustomAttributeNamedArgument_t3059612989 )L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsValidItem(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_IsValidItem_m2031032185_MetadataUsageId;
extern "C"  bool Collection_1_IsValidItem_m2031032185_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_IsValidItem_m2031032185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___item0;
		if (((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))
		{
			goto IL_0028;
		}
	}
	{
		Il2CppObject * L_1 = ___item0;
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		bool L_3 = Type_get_IsValueType_m1914757235((Type_t *)L_2, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B4_0 = 0;
	}

IL_0026:
	{
		G_B6_0 = G_B4_0;
		goto IL_0029;
	}

IL_0028:
	{
		G_B6_0 = 1;
	}

IL_0029:
	{
		return (bool)G_B6_0;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ConvertItem(System.Object)
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t Collection_1_ConvertItem_m4174059707_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t3059612989  Collection_1_ConvertItem_m4174059707_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_ConvertItem_m4174059707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___item0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_2 = ___item0;
		return ((*(CustomAttributeNamedArgument_t3059612989 *)((CustomAttributeNamedArgument_t3059612989 *)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)))));
	}

IL_0012:
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_CheckWritable_m240037625_MetadataUsageId;
extern "C"  void Collection_1_CheckWritable_m240037625_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_CheckWritable_m240037625_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___list0;
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1732551818 * L_2 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1__ctor_m3374324647_MetadataUsageId;
extern "C"  void Collection_1__ctor_m3374324647_gshared (Collection_1_t2486751580 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1__ctor_m3374324647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t374511678 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		List_1_t374511678 * L_0 = (List_1_t374511678 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t374511678 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		V_0 = (List_1_t374511678 *)L_0;
		List_1_t374511678 * L_1 = V_0;
		V_1 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_1;
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(1 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_syncRoot_1(L_3);
		List_1_t374511678 * L_4 = V_0;
		__this->set_list_0(L_4);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4216526896_gshared (Collection_1_t2486751580 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_System_Collections_ICollection_CopyTo_m4131878781_MetadataUsageId;
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m4131878781_gshared (Collection_1_t2486751580 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_System_Collections_ICollection_CopyTo_m4131878781_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t2643922881_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t2643922881_il2cpp_TypeInfo_var)), (Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1044491980_gshared (Collection_1_t2486751580 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2609273073_gshared (Collection_1_t2486751580 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Il2CppObject * L_3 = ___value0;
		CustomAttributeTypedArgument_t3301293422  L_4 = ((  CustomAttributeTypedArgument_t3301293422  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t2486751580 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t3301293422  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2486751580 *)__this, (int32_t)L_2, (CustomAttributeTypedArgument_t3301293422 )L_4);
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1738786543_gshared (Collection_1_t2486751580 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, CustomAttributeTypedArgument_t3301293422  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_2, (CustomAttributeTypedArgument_t3301293422 )((*(CustomAttributeTypedArgument_t3301293422 *)((CustomAttributeTypedArgument_t3301293422 *)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m4246646473_gshared (Collection_1_t2486751580 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t3301293422  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_2, (CustomAttributeTypedArgument_t3301293422 )((*(CustomAttributeTypedArgument_t3301293422 *)((CustomAttributeTypedArgument_t3301293422 *)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m890770108_gshared (Collection_1_t2486751580 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		CustomAttributeTypedArgument_t3301293422  L_2 = ((  CustomAttributeTypedArgument_t3301293422  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t2486751580 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t3301293422  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2486751580 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t3301293422 )L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1088935148_gshared (Collection_1_t2486751580 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		Il2CppObject * L_1 = ___value0;
		CustomAttributeTypedArgument_t3301293422  L_2 = ((  CustomAttributeTypedArgument_t3301293422  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t2486751580 *)__this);
		int32_t L_3 = ((  int32_t (*) (Collection_1_t2486751580 *, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Collection_1_t2486751580 *)__this, (CustomAttributeTypedArgument_t3301293422 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		NullCheck((Collection_1_t2486751580 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2486751580 *)__this, (int32_t)L_4);
		return;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m639609663_gshared (Collection_1_t2486751580 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_1();
		return L_0;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1550174470_gshared (Collection_1_t2486751580 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeTypedArgument_t3301293422  L_2 = InterfaceFuncInvoker1< CustomAttributeTypedArgument_t3301293422 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		CustomAttributeTypedArgument_t3301293422  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1945534355_gshared (Collection_1_t2486751580 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		CustomAttributeTypedArgument_t3301293422  L_2 = ((  CustomAttributeTypedArgument_t3301293422  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		NullCheck((Collection_1_t2486751580 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t3301293422  >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::SetItem(System.Int32,T) */, (Collection_1_t2486751580 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t3301293422 )L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void Collection_1_Add_m1900106744_gshared (Collection_1_t2486751580 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		CustomAttributeTypedArgument_t3301293422  L_3 = ___item0;
		NullCheck((Collection_1_t2486751580 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t3301293422  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2486751580 *)__this, (int32_t)L_2, (CustomAttributeTypedArgument_t3301293422 )L_3);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void Collection_1_Clear_m780457938_gshared (Collection_1_t2486751580 * __this, const MethodInfo* method)
{
	{
		NullCheck((Collection_1_t2486751580 *)__this);
		VirtActionInvoker0::Invoke(30 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ClearItems() */, (Collection_1_t2486751580 *)__this);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3819887728_gshared (Collection_1_t2486751580 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Clear() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool Collection_1_Contains_m2837323972_gshared (Collection_1_t2486751580 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeTypedArgument_t3301293422  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, CustomAttributeTypedArgument_t3301293422  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (CustomAttributeTypedArgument_t3301293422 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1256664552_gshared (Collection_1_t2486751580 * __this, CustomAttributeTypedArgumentU5BU5D_t2088020251* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< CustomAttributeTypedArgumentU5BU5D_t2088020251*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3945102107_gshared (Collection_1_t2486751580 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3914113972_gshared (Collection_1_t2486751580 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeTypedArgument_t3301293422  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t3301293422  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (CustomAttributeTypedArgument_t3301293422 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1153932895_gshared (Collection_1_t2486751580 * __this, int32_t ___index0, CustomAttributeTypedArgument_t3301293422  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgument_t3301293422  L_1 = ___item1;
		NullCheck((Collection_1_t2486751580 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t3301293422  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2486751580 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t3301293422 )L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2730132754_gshared (Collection_1_t2486751580 * __this, int32_t ___index0, CustomAttributeTypedArgument_t3301293422  ___item1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		CustomAttributeTypedArgument_t3301293422  L_2 = ___item1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< int32_t, CustomAttributeTypedArgument_t3301293422  >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1, (CustomAttributeTypedArgument_t3301293422 )L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool Collection_1_Remove_m1647067583_gshared (Collection_1_t2486751580 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		CustomAttributeTypedArgument_t3301293422  L_0 = ___item0;
		NullCheck((Collection_1_t2486751580 *)__this);
		int32_t L_1 = ((  int32_t (*) (Collection_1_t2486751580 *, CustomAttributeTypedArgument_t3301293422 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Collection_1_t2486751580 *)__this, (CustomAttributeTypedArgument_t3301293422 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		return (bool)0;
	}

IL_0011:
	{
		int32_t L_3 = V_0;
		NullCheck((Collection_1_t2486751580 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2486751580 *)__this, (int32_t)L_3);
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3322753061_gshared (Collection_1_t2486751580 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((Collection_1_t2486751580 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2486751580 *)__this, (int32_t)L_0);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m609648389_gshared (Collection_1_t2486751580 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		return;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m895756359_gshared (Collection_1_t2486751580 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeTypedArgument_t3301293422  Collection_1_get_Item_m2534531275_gshared (Collection_1_t2486751580 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeTypedArgument_t3301293422  L_2 = InterfaceFuncInvoker1< CustomAttributeTypedArgument_t3301293422 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2637546358_gshared (Collection_1_t2486751580 * __this, int32_t ___index0, CustomAttributeTypedArgument_t3301293422  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgument_t3301293422  L_1 = ___value1;
		NullCheck((Collection_1_t2486751580 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t3301293422  >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::SetItem(System.Int32,T) */, (Collection_1_t2486751580 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t3301293422 )L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2728771139_gshared (Collection_1_t2486751580 * __this, int32_t ___index0, CustomAttributeTypedArgument_t3301293422  ___item1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		CustomAttributeTypedArgument_t3301293422  L_2 = ___item1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< int32_t, CustomAttributeTypedArgument_t3301293422  >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1, (CustomAttributeTypedArgument_t3301293422 )L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IsValidItem(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_IsValidItem_m3654037736_MetadataUsageId;
extern "C"  bool Collection_1_IsValidItem_m3654037736_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_IsValidItem_m3654037736_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___item0;
		if (((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))
		{
			goto IL_0028;
		}
	}
	{
		Il2CppObject * L_1 = ___item0;
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		bool L_3 = Type_get_IsValueType_m1914757235((Type_t *)L_2, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B4_0 = 0;
	}

IL_0026:
	{
		G_B6_0 = G_B4_0;
		goto IL_0029;
	}

IL_0028:
	{
		G_B6_0 = 1;
	}

IL_0029:
	{
		return (bool)G_B6_0;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ConvertItem(System.Object)
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t Collection_1_ConvertItem_m1502097962_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t3301293422  Collection_1_ConvertItem_m1502097962_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_ConvertItem_m1502097962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___item0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_2 = ___item0;
		return ((*(CustomAttributeTypedArgument_t3301293422 *)((CustomAttributeTypedArgument_t3301293422 *)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)))));
	}

IL_0012:
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_CheckWritable_m2442447784_MetadataUsageId;
extern "C"  void Collection_1_CheckWritable_m2442447784_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_CheckWritable_m2442447784_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___list0;
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1732551818 * L_2 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::.ctor(System.Collections.Generic.IList`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322014;
extern const uint32_t ReadOnlyCollection_1__ctor_m1366664402_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1__ctor_m1366664402_gshared (ReadOnlyCollection_1_t1432926611 * __this, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1__ctor_m1366664402_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___list0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3322014, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Il2CppObject* L_2 = ___list0;
		__this->set_list_0(L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared (ReadOnlyCollection_1_t1432926611 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.Clear()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared (ReadOnlyCollection_1_t1432926611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared (ReadOnlyCollection_1_t1432926611 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.Remove(T)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_MetadataUsageId;
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared (ReadOnlyCollection_1_t1432926611 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared (ReadOnlyCollection_1_t1432926611 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared (ReadOnlyCollection_1_t1432926611 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((ReadOnlyCollection_1_t1432926611 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1432926611 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ReadOnlyCollection_1_t1432926611 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared (ReadOnlyCollection_1_t1432926611 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared (ReadOnlyCollection_1_t1432926611 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared (ReadOnlyCollection_1_t1432926611 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t2643922881_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t2643922881_il2cpp_TypeInfo_var)), (Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_MetadataUsageId;
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared (ReadOnlyCollection_1_t1432926611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_MetadataUsageId;
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared (ReadOnlyCollection_1_t1432926611 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Clear()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared (ReadOnlyCollection_1_t1432926611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared (ReadOnlyCollection_1_t1432926611 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2, (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared (ReadOnlyCollection_1_t1432926611 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_2, (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared (ReadOnlyCollection_1_t1432926611 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared (ReadOnlyCollection_1_t1432926611 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.RemoveAt(System.Int32)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared (ReadOnlyCollection_1_t1432926611 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared (ReadOnlyCollection_1_t1432926611 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared (ReadOnlyCollection_1_t1432926611 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared (ReadOnlyCollection_1_t1432926611 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m687553276_gshared (ReadOnlyCollection_1_t1432926611 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m475587820_gshared (ReadOnlyCollection_1_t1432926611 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		ObjectU5BU5D_t1108656482* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< ObjectU5BU5D_t1108656482*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0, (ObjectU5BU5D_t1108656482*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m809369055_gshared (ReadOnlyCollection_1_t1432926611 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m817393776_gshared (ReadOnlyCollection_1_t1432926611 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3681678091_gshared (ReadOnlyCollection_1_t1432926611 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_get_Item_m2421641197_gshared (ReadOnlyCollection_1_t1432926611 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.IList`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322014;
extern const uint32_t ReadOnlyCollection_1__ctor_m3466118433_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1__ctor_m3466118433_gshared (ReadOnlyCollection_1_t321723229 * __this, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1__ctor_m3466118433_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___list0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3322014, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Il2CppObject* L_2 = ___list0;
		__this->set_list_0(L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Add(T)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2455993995_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2455993995_gshared (ReadOnlyCollection_1_t321723229 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2455993995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Clear()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m833093535_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m833093535_gshared (ReadOnlyCollection_1_t321723229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m833093535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1011322802_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1011322802_gshared (ReadOnlyCollection_1_t321723229 * __this, int32_t ___index0, CustomAttributeNamedArgument_t3059612989  ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1011322802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Remove(T)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1374717388_MetadataUsageId;
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1374717388_gshared (ReadOnlyCollection_1_t321723229 * __this, CustomAttributeNamedArgument_t3059612989  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1374717388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3180142968_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3180142968_gshared (ReadOnlyCollection_1_t321723229 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3180142968_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  CustomAttributeNamedArgument_t3059612989  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2735326366_gshared (ReadOnlyCollection_1_t321723229 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((ReadOnlyCollection_1_t321723229 *)__this);
		CustomAttributeNamedArgument_t3059612989  L_1 = ((  CustomAttributeNamedArgument_t3059612989  (*) (ReadOnlyCollection_1_t321723229 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ReadOnlyCollection_1_t321723229 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3028200457_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3028200457_gshared (ReadOnlyCollection_1_t321723229 * __this, int32_t ___index0, CustomAttributeNamedArgument_t3059612989  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3028200457_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3502725699_gshared (ReadOnlyCollection_1_t321723229 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m58572112_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m58572112_gshared (ReadOnlyCollection_1_t321723229 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m58572112_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t2643922881_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t2643922881_il2cpp_TypeInfo_var)), (Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3974072671_MetadataUsageId;
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3974072671_gshared (ReadOnlyCollection_1_t321723229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3974072671_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2157369662_MetadataUsageId;
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2157369662_gshared (ReadOnlyCollection_1_t321723229 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Add_m2157369662_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Clear()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Clear_m1270090846_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1270090846_gshared (ReadOnlyCollection_1_t321723229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Clear_m1270090846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m4167890114_gshared (ReadOnlyCollection_1_t321723229 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, CustomAttributeNamedArgument_t3059612989  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2, (CustomAttributeNamedArgument_t3059612989 )((*(CustomAttributeNamedArgument_t3059612989 *)((CustomAttributeNamedArgument_t3059612989 *)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3928768662_gshared (ReadOnlyCollection_1_t321723229 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t3059612989  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_2, (CustomAttributeNamedArgument_t3059612989 )((*(CustomAttributeNamedArgument_t3059612989 *)((CustomAttributeNamedArgument_t3059612989 *)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Insert_m3096196361_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3096196361_gshared (ReadOnlyCollection_1_t321723229 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Insert_m3096196361_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Remove_m1439234431_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1439234431_gshared (ReadOnlyCollection_1_t321723229 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Remove_m1439234431_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.RemoveAt(System.Int32)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3251950105_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3251950105_gshared (ReadOnlyCollection_1_t321723229 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3251950105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4281934668_gshared (ReadOnlyCollection_1_t321723229 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1426158035_gshared (ReadOnlyCollection_1_t321723229 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeNamedArgument_t3059612989  L_2 = InterfaceFuncInvoker1< CustomAttributeNamedArgument_t3059612989 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)L_1);
		CustomAttributeNamedArgument_t3059612989  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_set_Item_m3941286560_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3941286560_gshared (ReadOnlyCollection_1_t321723229 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_set_Item_m3941286560_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3072511761_gshared (ReadOnlyCollection_1_t321723229 * __this, CustomAttributeNamedArgument_t3059612989  ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeNamedArgument_t3059612989  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, CustomAttributeNamedArgument_t3059612989  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0, (CustomAttributeNamedArgument_t3059612989 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2591949499_gshared (ReadOnlyCollection_1_t321723229 * __this, CustomAttributeNamedArgumentU5BU5D_t1983528240* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< CustomAttributeNamedArgumentU5BU5D_t1983528240*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0, (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2795667688_gshared (ReadOnlyCollection_1_t321723229 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2162782663_gshared (ReadOnlyCollection_1_t321723229 * __this, CustomAttributeNamedArgument_t3059612989  ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeNamedArgument_t3059612989  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t3059612989  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (CustomAttributeNamedArgument_t3059612989 )L_1);
		return L_2;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2439060628_gshared (ReadOnlyCollection_1_t321723229 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeNamedArgument_t3059612989  ReadOnlyCollection_1_get_Item_m719412574_gshared (ReadOnlyCollection_1_t321723229 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeNamedArgument_t3059612989  L_2 = InterfaceFuncInvoker1< CustomAttributeNamedArgument_t3059612989 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.IList`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322014;
extern const uint32_t ReadOnlyCollection_1__ctor_m713162960_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1__ctor_m713162960_gshared (ReadOnlyCollection_1_t563403662 * __this, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1__ctor_m713162960_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___list0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3322014, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Il2CppObject* L_2 = ___list0;
		__this->set_list_0(L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Add(T)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m450448058_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m450448058_gshared (ReadOnlyCollection_1_t563403662 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m450448058_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Clear()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3085678928_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3085678928_gshared (ReadOnlyCollection_1_t563403662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3085678928_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m183184673_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m183184673_gshared (ReadOnlyCollection_1_t563403662 * __this, int32_t ___index0, CustomAttributeTypedArgument_t3301293422  ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m183184673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Remove(T)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1445762877_MetadataUsageId;
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1445762877_gshared (ReadOnlyCollection_1_t563403662 * __this, CustomAttributeTypedArgument_t3301293422  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1445762877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2352004839_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2352004839_gshared (ReadOnlyCollection_1_t563403662 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2352004839_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  CustomAttributeTypedArgument_t3301293422  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1907188237_gshared (ReadOnlyCollection_1_t563403662 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((ReadOnlyCollection_1_t563403662 *)__this);
		CustomAttributeTypedArgument_t3301293422  L_1 = ((  CustomAttributeTypedArgument_t3301293422  (*) (ReadOnlyCollection_1_t563403662 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ReadOnlyCollection_1_t563403662 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1756408248_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1756408248_gshared (ReadOnlyCollection_1_t563403662 * __this, int32_t ___index0, CustomAttributeTypedArgument_t3301293422  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1756408248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2674587570_gshared (ReadOnlyCollection_1_t563403662 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m772492159_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m772492159_gshared (ReadOnlyCollection_1_t563403662 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m772492159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t2643922881_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t2643922881_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t2643922881_il2cpp_TypeInfo_var)), (Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4083717454_MetadataUsageId;
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4083717454_gshared (ReadOnlyCollection_1_t563403662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4083717454_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2345659311_MetadataUsageId;
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2345659311_gshared (ReadOnlyCollection_1_t563403662 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Add_m2345659311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Clear()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Clear_m3595441805_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3595441805_gshared (ReadOnlyCollection_1_t563403662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Clear_m3595441805_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2162344177_gshared (ReadOnlyCollection_1_t563403662 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, CustomAttributeTypedArgument_t3301293422  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2, (CustomAttributeTypedArgument_t3301293422 )((*(CustomAttributeTypedArgument_t3301293422 *)((CustomAttributeTypedArgument_t3301293422 *)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3032789639_gshared (ReadOnlyCollection_1_t563403662 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t3301293422  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_2, (CustomAttributeTypedArgument_t3301293422 )((*(CustomAttributeTypedArgument_t3301293422 *)((CustomAttributeTypedArgument_t3301293422 *)UnBox (L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Insert_m3193718138_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3193718138_gshared (ReadOnlyCollection_1_t563403662 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Insert_m3193718138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Remove_m1548879214_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1548879214_gshared (ReadOnlyCollection_1_t563403662 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Remove_m1548879214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.RemoveAt(System.Int32)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2355971082_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2355971082_gshared (ReadOnlyCollection_1_t563403662 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2355971082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m544693629_gshared (ReadOnlyCollection_1_t563403662 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m530179012_gshared (ReadOnlyCollection_1_t563403662 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeTypedArgument_t3301293422  L_2 = InterfaceFuncInvoker1< CustomAttributeTypedArgument_t3301293422 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)L_1);
		CustomAttributeTypedArgument_t3301293422  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_set_Item_m3170433745_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3170433745_gshared (ReadOnlyCollection_1_t563403662 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_set_Item_m3170433745_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3355043202_gshared (ReadOnlyCollection_1_t563403662 * __this, CustomAttributeTypedArgument_t3301293422  ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeTypedArgument_t3301293422  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, CustomAttributeTypedArgument_t3301293422  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0, (CustomAttributeTypedArgument_t3301293422 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2705370090_gshared (ReadOnlyCollection_1_t563403662 * __this, CustomAttributeTypedArgumentU5BU5D_t2088020251* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< CustomAttributeTypedArgumentU5BU5D_t2088020251*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0, (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2956392153_gshared (ReadOnlyCollection_1_t563403662 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3834464566_gshared (ReadOnlyCollection_1_t563403662 * __this, CustomAttributeTypedArgument_t3301293422  ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeTypedArgument_t3301293422  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t3301293422  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (CustomAttributeTypedArgument_t3301293422 )L_1);
		return L_2;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2721592069_gshared (ReadOnlyCollection_1_t563403662 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeTypedArgument_t3301293422  ReadOnlyCollection_1_get_Item_m2806679117_gshared (ReadOnlyCollection_1_t563403662 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeTypedArgument_t3301293422  L_2 = InterfaceFuncInvoker1< CustomAttributeTypedArgument_t3301293422 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Comparison`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m487232819_gshared (Comparison_1_t2887177558 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<System.Object>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m1888033133_gshared (Comparison_1_t2887177558 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m1888033133((Comparison_1_t2887177558 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Comparison`1<System.Object>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m3177996774_gshared (Comparison_1_t2887177558 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___x0;
	__d_args[1] = ___y1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 System.Comparison`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m651541983_gshared (Comparison_1_t2887177558 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Converter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Converter_2__ctor_m15321797_gshared (Converter_2_t3715610867 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TOutput System.Converter`2<System.Object,System.Object>::Invoke(TInput)
extern "C"  Il2CppObject * Converter_2_Invoke_m606895179_gshared (Converter_2_t3715610867 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Converter_2_Invoke_m606895179((Converter_2_t3715610867 *)__this->get_prev_9(),___input0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Converter`2<System.Object,System.Object>::BeginInvoke(TInput,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Converter_2_BeginInvoke_m3132354088_gshared (Converter_2_t3715610867 * __this, Il2CppObject * ___input0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___input0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TOutput System.Converter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Converter_2_EndInvoke_m3873471959_gshared (Converter_2_t3715610867 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::.ctor()
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m3915480592_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2518344089_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m1460175968_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m416466113_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1980624040_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 * __this, const MethodInfo* method)
{
	U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 * L_2 = (U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 *)L_2;
		U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 * L_3 = V_0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CU24U3Esource_5();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m1129579948_MetadataUsageId;
extern "C"  bool U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m1129579948_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m1129579948_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00af;
	}

IL_0023:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_source_0();
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_U3CU24s_59U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0078;
			}
		}

IL_0043:
		{
			goto IL_0078;
		}

IL_0048:
		{
			Il2CppObject * L_5 = (Il2CppObject *)__this->get_U3CU24s_59U3E__0_1();
			NullCheck((Il2CppObject *)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			__this->set_U3CelementU3E__1_2(((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))));
			Il2CppObject * L_7 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_4(L_7);
			__this->set_U24PC_3(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB1, FINALLY_008d);
		}

IL_0078:
		{
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CU24s_59U3E__0_1();
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_0048;
			}
		}

IL_0088:
		{
			IL2CPP_LEAVE(0xA8, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			bool L_10 = V_1;
			if (!L_10)
			{
				goto IL_0091;
			}
		}

IL_0090:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_0091:
		{
			Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CU24s_59U3E__0_1();
			V_2 = (Il2CppObject *)((Il2CppObject *)IsInst(L_11, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			Il2CppObject * L_12 = V_2;
			if (L_12)
			{
				goto IL_00a1;
			}
		}

IL_00a0:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_00a1:
		{
			Il2CppObject * L_13 = V_2;
			NullCheck((Il2CppObject *)L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0xB1, IL_00b1)
		IL2CPP_JUMP_TBL(0xA8, IL_00a8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00a8:
	{
		__this->set_U24PC_3((-1));
	}

IL_00af:
	{
		return (bool)0;
	}

IL_00b1:
	{
		return (bool)1;
	}
	// Dead block : IL_00b3: ldloc.3
}
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m283780685_MetadataUsageId;
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m283780685_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m283780685_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003d;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = (Il2CppObject *)__this->get_U3CU24s_59U3E__0_1();
			V_1 = (Il2CppObject *)((Il2CppObject *)IsInst(L_2, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			Il2CppObject * L_3 = V_1;
			if (L_3)
			{
				goto IL_0036;
			}
		}

IL_0035:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_0036:
		{
			Il2CppObject * L_4 = V_1;
			NullCheck((Il2CppObject *)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m1561913533_MetadataUsageId;
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m1561913533_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3934985649 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m1561913533_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C"  void Nullable_1__ctor_m4008503583_gshared (Nullable_1_t497649510 * __this, TimeSpan_t413522987  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		TimeSpan_t413522987  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m4008503583_AdjustorThunk (Il2CppObject * __this, TimeSpan_t413522987  ___value0, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m4008503583(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2797118855_gshared (Nullable_1_t497649510 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m2797118855_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m2797118855(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3338249190_MetadataUsageId;
extern "C"  TimeSpan_t413522987  Nullable_1_get_Value_m3338249190_gshared (Nullable_1_t497649510 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3338249190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		TimeSpan_t413522987  L_2 = (TimeSpan_t413522987 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  TimeSpan_t413522987  Nullable_1_get_Value_m3338249190_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	TimeSpan_t413522987  _returnValue = Nullable_1_get_Value_m3338249190(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2158814990_gshared (Nullable_1_t497649510 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t497649510 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Il2CppObject *, Nullable_1_t497649510 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((Nullable_1_t497649510 *)__this), (Nullable_1_t497649510 )((*(Nullable_1_t497649510 *)((Nullable_1_t497649510 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m2158814990_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2158814990(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3609411697_gshared (Nullable_1_t497649510 * __this, Nullable_1_t497649510  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		TimeSpan_t413522987 * L_3 = (TimeSpan_t413522987 *)(&___other0)->get_address_of_value_0();
		TimeSpan_t413522987  L_4 = (TimeSpan_t413522987 )__this->get_value_0();
		TimeSpan_t413522987  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = TimeSpan_Equals_m2969422609((TimeSpan_t413522987 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3609411697_AdjustorThunk (Il2CppObject * __this, Nullable_1_t497649510  ___other0, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3609411697(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2957066482_gshared (Nullable_1_t497649510 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		TimeSpan_t413522987 * L_1 = (TimeSpan_t413522987 *)__this->get_address_of_value_0();
		int32_t L_2 = TimeSpan_GetHashCode_m3188156777((TimeSpan_t413522987 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m2957066482_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m2957066482(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3059865940_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3059865940_gshared (Nullable_1_t497649510 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3059865940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		TimeSpan_t413522987 * L_1 = (TimeSpan_t413522987 *)__this->get_address_of_value_0();
		String_t* L_2 = TimeSpan_ToString_m2803989647((TimeSpan_t413522987 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m3059865940_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t497649510  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m3059865940(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t413522987 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m982040097_gshared (Predicate_1_t3781873254 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Object>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4106178309_gshared (Predicate_1_t3781873254 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m4106178309((Predicate_1_t3781873254 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2038073176_gshared (Predicate_1_t3781873254 * __this, Il2CppObject * ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3970497007_gshared (Predicate_1_t3781873254 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m207645679_gshared (Predicate_1_t6933607 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1510075547_gshared (Predicate_1_t6933607 * __this, TrackableResultData_t395876724  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1510075547((Predicate_1_t6933607 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, TrackableResultData_t395876724  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, TrackableResultData_t395876724  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* TrackableResultData_t395876724_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2760591914_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2760591914_gshared (Predicate_1_t6933607 * __this, TrackableResultData_t395876724  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2760591914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(TrackableResultData_t395876724_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2591413785_gshared (Predicate_1_t6933607 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetResultData>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3870936763_gshared (Predicate_1_t2549574927 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetResultData>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1857252419_gshared (Predicate_1_t2549574927 * __this, VuMarkTargetResultData_t2938518044  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1857252419((Predicate_1_t2549574927 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, VuMarkTargetResultData_t2938518044  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, VuMarkTargetResultData_t2938518044  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetResultData>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* VuMarkTargetResultData_t2938518044_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3896538518_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3896538518_gshared (Predicate_1_t2549574927 * __this, VuMarkTargetResultData_t2938518044  ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3896538518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(VuMarkTargetResultData_t2938518044_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetResultData>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2914791281_gshared (Predicate_1_t2549574927 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Getter_2__ctor_m4236926794_gshared (Getter_2_t2712548107 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Getter_2_Invoke_m410564889_gshared (Getter_2_t2712548107 * __this, Il2CppObject * ____this0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Getter_2_Invoke_m410564889((Getter_2_t2712548107 *)__this->get_prev_9(),____this0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Getter_2_BeginInvoke_m3146221447_gshared (Getter_2_t2712548107 * __this, Il2CppObject * ____this0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____this0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Getter_2_EndInvoke_m1749574747_gshared (Getter_2_t2712548107 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Reflection.MonoProperty/StaticGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void StaticGetter_1__ctor_m3357261135_gshared (StaticGetter_1_t3245381133 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * StaticGetter_1_Invoke_m3410367530_gshared (StaticGetter_1_t3245381133 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StaticGetter_1_Invoke_m3410367530((StaticGetter_1_t3245381133 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StaticGetter_1_BeginInvoke_m3837643130_gshared (StaticGetter_1_t3245381133 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * StaticGetter_1_EndInvoke_m3212189152_gshared (StaticGetter_1_t3245381133 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m4139691420_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m4139691420_gshared (UnityEvent_1_t62446588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m4139691420_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_2__ctor_m1950601551_MetadataUsageId;
extern "C"  void UnityEvent_2__ctor_m1950601551_gshared (UnityEvent_2_t2262335274 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_2__ctor_m1950601551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_3__ctor_m4248091138_MetadataUsageId;
extern "C"  void UnityEvent_3__ctor_m4248091138_gshared (UnityEvent_3_t1668724050 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_3__ctor_m4248091138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)3)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_4__ctor_m492943285_MetadataUsageId;
extern "C"  void UnityEvent_4__ctor_m492943285_gshared (UnityEvent_4_t2186133700 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_4__ctor_m492943285_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4)));
		NullCheck((UnityEventBase_t1020378628 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t1020378628 *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
